//
//  ProfileArtemisaVC.swift
//  Artemisa
//
//  Created by Arturo Escamilla on 15/07/20.
//  Copyright © 2020 Microquasar. All rights reserved.
//

import UIKit

class ProfileArtemisaTViewCell: UITableViewCell
{
    
    @IBOutlet weak var lbl_name: UILabel!



}

class ProfileArtemisaVC: UIViewController,UITableViewDelegate, UITableViewDataSource,UIAdaptivePresentationControllerDelegate, ProfileArtemisaVCDelegate  {
   
    
    
    let alertmanager = AlertManager()
    
    @IBOutlet weak var tv_profile: UITableView!
    @IBOutlet weak var lbl_email: UILabel!
    
    @IBOutlet weak var lbl_company_name: UILabel!
    
    var profile_optiions = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let email = GlobalMembers.preferences.string(forKey: GlobalMembers.emailArtemisaKey)
                lbl_email.text = email
                tv_profile.backgroundColor = UIColor.white
                
                let saved_company = GlobalMembers.preferences.dictionary(forKey: GlobalMembers.companyArtemisaKey)
                if(saved_company != nil)
                {
                    let comp_shortname = saved_company!["company_shortname"] as! String
                    lbl_company_name.text = comp_shortname
                    
                }
        //        let opt1 = NSMutableDictionary()
        //        opt1.setValue("Cambiar contraseña", forKey: "name")
        //        profile_optiions.add(opt1)
                
                let opt2 = NSMutableDictionary()
                opt2.setValue("Seleccionar empresa", forKey: "name")
                profile_optiions.add(opt2)
                
                let opt3 = NSMutableDictionary()
                opt3.setValue("Salir de Artemisa", forKey: "name")
                profile_optiions.add(opt3)
                
                let opt4 = NSMutableDictionary()
                opt4.setValue("Cerrar sesión Artemisa", forKey: "name")
                profile_optiions.add(opt4)

        // Do any additional setup after loading the view.
    }
    
    // Cargar tabla
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return profile_optiions.count
       }
          
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cell = tableView.dequeueReusableCell(withIdentifier: "profile_cell", for: indexPath) as! ProfileArtemisaTViewCell
                cell.backgroundColor = UIColor.white
                cell.backgroundView?.backgroundColor = UIColor.white
                cell.tintColor = UIColor.white

                let image = UIImage(named: "img_arrow")
                let checkmark  = UIImageView(frame:CGRect(x:0, y:0, width:(image?.size.width)!, height:(image?.size.height)!));
                checkmark.image = image
                cell.accessoryView = checkmark
           
                let name = profile_optiions[indexPath.row] as! NSDictionary
                cell.lbl_name.text = name.object(forKey: "name") as? String

               if(indexPath.row == profile_optiions.count - 1)
               {
                   cell.lbl_name.textColor = UIColor(red: 50/255.0, green: 118/255.0, blue: 177/255.0, alpha: 1.0)
               }
                
                return cell
       }
       
       func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           
           switch indexPath.row {
           case 0:
                self.performSegue(withIdentifier: "segue_companies_artemisa", sender: nil)
               break
          case 1:
                self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
          break
           case 2:
               
               let sessionAlert = UIAlertController(title: "Sparkline", message: "¿Deseas cerrar sessión?", preferredStyle: UIAlertController.Style.alert)

               sessionAlert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { (action: UIAlertAction!) in
                  
                  
                       DispatchQueue.main.async {
                           //write your code here
                           GlobalMembers.preferences.set(nil, forKey: GlobalMembers.tokenArtemisaKey)
                           GlobalMembers.preferences.set(nil, forKey: GlobalMembers.emailArtemisaKey)
                           GlobalMembers.preferences.set(nil, forKey: GlobalMembers.companyArtemisaKey)
                           let didSave = GlobalMembers.preferences.synchronize()
                           if didSave {
                           // your code here
                               self.dismiss(animated: true, completion: nil)
                              
                               
                           }
                        }
                   
                 }))

               sessionAlert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: { (action: UIAlertAction!) in
              
                 }))

               present(sessionAlert, animated: true, completion: nil)
              
               break
           default:
               break
           }
           
       }
       
       func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return 66
       }

     
       // MARK: - Navigation

       // In a storyboard-based application, you will often want to do a little preparation before navigation
       override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
           // Get the new view controller using segue.destination.
           // Pass the selected object to the new view controller.
           
           if segue.identifier == "segue_companies_artemisa" {
               
               if let presented = segue.destination as? CompaniesArtemisaVC {
                 segue.destination.presentationController?.delegate = self;
                  presented.delegate = self
                  //anything else you do already
               }
           }
       }
    
       public func presentationControllerDidDismiss(
         _ presentationController: UIPresentationController)
       {
         let saved_company = GlobalMembers.preferences.dictionary(forKey: GlobalMembers.companyArtemisaKey)
         if(saved_company != nil)
         {
             let comp_shortname = saved_company!["company_shortname"] as! String
             lbl_company_name.text = comp_shortname
           
               if let index = self.tv_profile.indexPathForSelectedRow{
                   self.tv_profile.deselectRow(at: index, animated: true)
               }
             
         }
        
       }
       func resfreshdata() {
           
           let saved_company = GlobalMembers.preferences.dictionary(forKey: GlobalMembers.companyArtemisaKey)
                if(saved_company != nil)
                {
                    let comp_shortname = saved_company!["company_shortname"] as! String
                    lbl_company_name.text = comp_shortname
                  
                      if let index = self.tv_profile.indexPathForSelectedRow{
                          self.tv_profile.deselectRow(at: index, animated: true)
                      }
                   self.tabBarController!.selectedIndex = 1
                    
                }
         
       }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
