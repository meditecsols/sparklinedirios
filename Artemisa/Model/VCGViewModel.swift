//
//  VCGViewModel.swift
//  Artemisa
//
//  Created by Arturo Escamilla on 13/04/20.
//  Copyright © 2020 Microquasar. All rights reserved.
//

import UIKit
import SwiftSpinner

enum VCGViewModelItemType {
    
    case ventas
    case clientes
    case cpc
    case compras
    case gastos
    case proveedor
    case cpp
    
}

protocol VCGViewModelItem {
    var type: VCGViewModelItemType { get }
    var sectionTitle: [NSDictionary] { get }
    var rowCount: Int { get }
    var isCollapsible: Bool { get }
    var isCollapsed: Bool { get set }
}
extension VCGViewModelItem {
    var rowCount: Int {
        return 1
    }
    
    var isCollapsible: Bool {
        return true
    }
}

class VCGViewModel: NSObject {
    
    
    var items = [VCGViewModelItem]()
    
    var reloadSections: ((_ section: Int) -> Void)?
    
    override init() {
        super.init()
        

        
        let invokeServ = InvokeService()
        let saved_company = GlobalMembers.preferences.dictionary(forKey: GlobalMembers.companyArtemisaKey)
        if(saved_company != nil)
        {
            
              // Obtener id de compañia
             let comp_id = saved_company!["company_id"] as! String
            SwiftSpinner.show("Cargando")
             // Peticion a api Artemisa
            invokeServ.requestVentasDatabyCompanyWith(company_id: comp_id) { (response, error) in
            
            if error == nil
            {
                
                // Obtener datos de Ventas
                
                let sales_array = response!["bi_sales_sumary_by_company_and_customer"] as! NSArray


                var sales_amount_total = 0.0
                for sales in sales_array {

                    let sales_dict = sales as! NSDictionary

                    let sales_amount = sales_dict["sales_amount"] as! String

                    let rat: Double = Double(sales_amount_total)
                    let ra: Double = Double(sales_amount)!
                    sales_amount_total = rat + ra


                }
                
                
            
                invokeServ.requestCXCByCompanyDataWith(company_id: comp_id) { (response, error) in
                                if error == nil
                                {
                                    
                                    
                                   // Obtener datos de Clientes
                                    
                                    let cxc_array = response!["bi_cxc_sumary_by_company_and_customer"] as! NSArray


                                    var receivable_amount_total = 0.0
                                    for cxc in cxc_array {

                                        let cxc_dict = cxc as! NSDictionary

                                        let receivable_amount = cxc_dict["receivable_amount"] as! String

                                        let rat: Double = Double(receivable_amount_total)
                                        let ra: Double = Double(receivable_amount)!
                                        receivable_amount_total = rat + ra


                                    }
                                  
                                    // Obtener datos de Proveedor

                                    invokeServ.requestCXPByCompanyDataWith(company_id: comp_id)
                                    { (response_cxp, error) in
                                          if error == nil
                                            {
                                                    //Agregar sección de Ventas
                                                           
                                                  
                                                    if(sales_array.count > 0)
                                                    {
                                                        let first5_array = NSMutableArray()
                                                        var i = 0
                                                        for sales in sales_array {
                                                            if(i<5)
                                                            {
                                                            let sales_dict = sales as! NSDictionary
                                                            first5_array.add(sales_dict)
                                                            }
                                                            
                                                            i = i+1
                                                        }
                                                        
                                                        //Crear diccionario para mostrar detalles
                                                        
                                                        let details_dic:NSMutableDictionary = [:]
                                                        
                                                         details_dic["customer_shortname"] = "Ver mas detalles"
                                                         details_dic["receivable_amount"] = ">"
                                                        
                                                        first5_array.add(details_dic)
                                                        
                                                        
                                                        //Crear diccionario para titulo de Header
                                                        
                                                        let section_ventas:NSMutableDictionary = [:]
                                                       
                                                        section_ventas["title"] = "Ventas"
                                                        section_ventas["amount"] = String(format:"%f", sales_amount_total)
                                                                   
                                                        let attributesItem_ventas = ProfileViewModeVentasItem(ventas:first5_array as! [NSMutableArray],section: [section_ventas])
                                                        self.items.append(attributesItem_ventas)
                                                    }
                                                    else
                                                    {
                                                         //Crear diccionario para titulo de Header
                                                        let first5_array = NSMutableArray()
                                                         let section_ventas:NSMutableDictionary = [:]
                                                        
                                                         section_ventas["title"] = "Ventas"
                                                         section_ventas["amount"] = "0.00"
                                                                    
                                                         let attributesItem_ventas = ProfileViewModeVentasItem(ventas:first5_array as! [NSMutableArray],section: [section_ventas])
                                                         self.items.append(attributesItem_ventas)
                                                        
                                                    }
                                                    
                                                    
                                                    //Agregar sección de Clientes
                                                    if(cxc_array.count > 0)
                                                    {
                                                        let first5_array = NSMutableArray()
                                                        var i = 0
                                                        for cxc in cxc_array {
                                                            if(i<5)
                                                            {
                                                            let cxc_dict = cxc as! NSDictionary
                                                            first5_array.add(cxc_dict)
                                                            }
                                                            
                                                            i = i+1
                                                        }
                                                        
                                                        //Crear diccionario para mostrar detalles
                                                        
                                                        let details_dic:NSMutableDictionary = [:]
                                                        
                                                         details_dic["customer_shortname"] = "Ver mas detalles"
                                                         details_dic["receivable_amount"] = ">"
                                                        
                                                        first5_array.add(details_dic)
                                                        
                                                        
                                                        //Crear diccionario para titulo de Header
                                                        
                                                        let section_clientes:NSMutableDictionary = [:]
                                                       
                                                        section_clientes["title"] = "CPC"
                                                        section_clientes["amount"] = String(format:"%f", receivable_amount_total)
                                                                   
                                                        let attributesItem_clientes = ProfileViewModeClientesItem(clientes:first5_array as! [NSMutableArray],section: [section_clientes])
                                                        self.items.append(attributesItem_clientes)
                                                    }
                                                    else
                                                    {
                                                         //Crear diccionario para titulo de Header
                                                        let first5_array = NSMutableArray()
                                                         let section_clientes:NSMutableDictionary = [:]
                                                        
                                                         section_clientes["title"] = "CPC"
                                                         section_clientes["amount"] = "0.00"
                                                                    
                                                         let attributesItem_clientes = ProfileViewModeClientesItem(clientes:first5_array as! [NSMutableArray],section: [section_clientes])
                                                         self.items.append(attributesItem_clientes)
                                                        
                                                    }
                                                    
                                                    //Agregar sección de CPC
                                                           
//                                                    let cpc_array = NSMutableArray()
//
//
//                                                    let section_cpc:NSMutableDictionary = [:]
//                                                    section_cpc["title"] = "CPC"
//                                                    section_cpc["amount"] = "0.00"
//                                                    let attributesItem_cpc = ProfileViewModeCPCItem(cpc:cpc_array as! [NSMutableArray],section: [section_cpc])
//                                                    self.items.append(attributesItem_cpc)
                                                    
                                                    
                                                    //Agregar sección de Compras
                                                           
                                                    let compras_array = NSMutableArray()
                                                    
                                                           
                                                    let section_compras:NSMutableDictionary = [:]
                                                    section_compras["title"] = "Compras"
                                                    section_compras["amount"] = "0.00"
                                                    let attributesItem_compras = ProfileViewModeComprasItem(compras:compras_array as! [NSMutableArray],section: [section_compras])
                                                    self.items.append(attributesItem_compras)
                                                    
                                                    

                                                    
                                                    
                                                    // Agregar sección de Gastos
                                                    let cxp_array = response_cxp!["bi_cxp_sumary_by_company_and_supplier"] as! NSArray
                                                    
                                                    if(cxp_array.count > 0)
                                                    {
                                                        var amount_to_pay_total: Double = 00.00
                                                        for cxp in cxp_array
                                                        {
                                                    
                                                                let cxp_dict = cxp as! NSDictionary
                                                                let amount_to_pay = cxp_dict["amount_to_pay"] as! String
                                                                let amtpt: Double = Double(amount_to_pay_total)
                                                                let amtp: Double = Double(amount_to_pay)!
                                                                                            amount_to_pay_total = amtpt + amtp
                                                    
                                                        }
                                                        
                                                        let first5_cxp_array = NSMutableArray()
                                                        var i = 0
                                                        for cxp in cxp_array {
                                                            if(i<5)
                                                            {
                                                            let cxp_dict = cxp as! NSDictionary
                                                            first5_cxp_array.add(cxp_dict)
                                                            }
                                                            
                                                            i = i+1
                                                        }
                                                        
                                                         //Crear diccionario para mostrar detalles
                                                         
                                                         let details_dic:NSMutableDictionary = [:]
                                                         
                                                          details_dic["supplier_shortname"] = "Ver mas detalles"
                                                          details_dic["payment_amount"] = ">"
                                                         
                                                         first5_cxp_array.add(details_dic)
                                                         
                                                         
                                                         //Crear diccionario para titulo de Header
                                                         
                                                         let section_proveedores:NSMutableDictionary = [:]
                                                        
                                                         section_proveedores["title"] = "Gastos"
                                                         section_proveedores["amount"] = String(format:"%f", amount_to_pay_total)
                                                                    
                                                         let attributesItem_proveedores = ProfileViewModeGastosItem(gastos:first5_cxp_array as! [NSMutableArray],section: [section_proveedores])
                                                         self.items.append(attributesItem_proveedores)
                                                    }
                                                    else
                                                    {
                                                         let first5_cxp_array = NSMutableArray()
                                                         //Crear diccionario para titulo de Header
                                                         
                                                         let section_proveedores:NSMutableDictionary = [:]
                                                        
                                                         section_proveedores["title"] = "Gastos"
                                                         section_proveedores["amount"] = "0.00"
                                                                    
                                                         let attributesItem_proveedores = ProfileViewModeGastosItem(gastos:first5_cxp_array as! [NSMutableArray],section: [section_proveedores])
                                                         self.items.append(attributesItem_proveedores)
                                                        
                                                    }
                                                
                                                    //Agregar sección de Gastos
                                                           
//                                                    let gastos_array = NSMutableArray()
//
//
//                                                    let section_gastos:NSMutableDictionary = [:]
//                                                    section_gastos["title"] = "CPP"
//                                                    section_gastos["amount"] = "0.00"
//                                                    let attributesItem_gastos = ProfileViewModeGastosItem(gastos:gastos_array as! [NSMutableArray],section: [section_gastos])
//                                                    self.items.append(attributesItem_gastos)
                                                    
                                                
                                                    //Agregar sección de CPP
                                                           
                                                    let cpp_array = NSMutableArray()
                                                    
                                                           
                                                    let section_cpp:NSMutableDictionary = [:]
                                                    section_cpp["title"] = "CPP"
                                                    section_cpp["amount"] = "0.00"
                                                    let attributesItem_cpp = ProfileViewModeCPPItem(cpp:cpp_array as! [NSMutableArray],section: [section_cpp])
                                                    self.items.append(attributesItem_cpp)
                                                
                                                
                                                    
                                                    DispatchQueue.main.async
                                                    {
                                                        SwiftSpinner.hide()
                                                        self.reloadSections!(0)
                                                        self.reloadSections!(1)
//                                                       self.reloadSections!(2)
                                                        self.reloadSections!(2)
                                                        self.reloadSections!(3)
                                                        self.reloadSections!(4)
                                                        //self.reloadSections!(5)
                                                        
                                                        
                                                    }
                                                    
                                                    
                                            }
                                        else
                                          {
                                            SwiftSpinner.hide()
                                        }
                                                
                                                
                                                
                                                
                                                
                                                
                                    }
                                            
                                            
                                            
                                            
                                            
                                            
                                           
                                        
                                       
                                        
                                    

                                }
                    else
                                {
                                    SwiftSpinner.hide()
                    }
            }

            }

            }

        }
        
        
        

        
    }

}


extension VCGViewModel: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (items.count == 0)
        {
             return 0
        }
        let item = items[section]
        guard item.isCollapsible else {
            return item.rowCount
        }
        
        if item.isCollapsed {
            return 0
        } else {
            return item.rowCount
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(indexPath.section == 4)
        {
            let item = items[indexPath.section]
            let item_prov = item as? ProfileViewModeProveedorItem
            if(indexPath.row == (item_prov?.proveedor.count)!-1)
            {
                if var topController = UIApplication.shared.keyWindow?.rootViewController {
                    while let presentedViewController = topController.presentedViewController {
                        topController = presentedViewController
                    }

                    topController.performSegue(withIdentifier: "segue_details", sender: nil)
                }
                
            }
            
            
        }
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.section]
        switch item.type {
        
        case .clientes:
            if let item = item as? ProfileViewModeClientesItem, let cell = tableView.dequeueReusableCell(withIdentifier: AttributeCell.identifier, for: indexPath) as? AttributeCell {
                cell.backgroundColor = UIColor.white
                let clientes_array = item.clientes as NSArray
                let dict_cliente = clientes_array[indexPath.row] as! NSDictionary
                cell.lbl_name.text = dict_cliente["customer_shortname"] as? String;
                let receivable_amount_double = Double(dict_cliente["receivable_amount"] as! String)
                if(receivable_amount_double != nil)
                {
                    
                    cell.lbl_amount.text = convertDoubleToCurrency(amount: receivable_amount_double!)
                }
                else
                {
                    cell.lbl_amount.text = ""
                }
               // cell.item = dic
                return cell
            }
        case .ventas:
            if let item = item as? ProfileViewModeVentasItem, let cell = tableView.dequeueReusableCell(withIdentifier: AttributeCell.identifier, for: indexPath) as? AttributeCell {
                 cell.backgroundColor = UIColor.white
                 let ventas_array = item.ventas as NSArray
                 let dict_ventas = ventas_array[indexPath.row] as! NSDictionary
                 cell.lbl_name.text = dict_ventas["customer_shortname"] as? String;
              
                var sales_amount_double : Double = 0
                if(dict_ventas["sales_amount"] != nil)
                {
                    sales_amount_double = Double(dict_ventas["sales_amount"] as! String)!
                    cell.lbl_amount.text = convertDoubleToCurrency(amount: sales_amount_double)
                }
                else
                {
                    cell.lbl_amount.text = ""
                }

                return cell
            }
        case .cpc:
            if let item = item as? ProfileViewModeCPCItem, let cell = tableView.dequeueReusableCell(withIdentifier: AttributeCell.identifier, for: indexPath) as? AttributeCell {
                cell.backgroundColor = UIColor.white
                let cpc_array = item.cpc as NSArray
                let dict_cpc = cpc_array[indexPath.row] as! NSDictionary
                return cell
            }
        case .compras:
            if let item = item as? ProfileViewModeComprasItem, let cell = tableView.dequeueReusableCell(withIdentifier: AttributeCell.identifier, for: indexPath) as? AttributeCell {
                let compras_array = item.compras as NSArray
                let dict_cpc = compras_array[indexPath.row] as! NSDictionary
                return cell
            }
        case .gastos:
            if let item = item as? ProfileViewModeGastosItem, let cell = tableView.dequeueReusableCell(withIdentifier: AttributeCell.identifier, for: indexPath) as? AttributeCell {
                cell.backgroundColor = UIColor.white
                let gastos_array = item.gastos as NSArray
                let dict_gastos = gastos_array[indexPath.row] as! NSDictionary
                cell.lbl_name.text = dict_gastos["supplier_shortname"] as? String
                               var payment_amount_double : Double = 0
                               if(dict_gastos["amount_to_pay"] != nil)
                               {
                                   payment_amount_double = Double(dict_gastos["amount_to_pay"] as! String)!
                                   cell.lbl_amount.text = convertDoubleToCurrency(amount: payment_amount_double)
                               }
                               else
                               {
                                   cell.lbl_amount.text = ""
                               }

                return cell
            }
        case .proveedor:
            if let item = item as? ProfileViewModeProveedorItem, let cell = tableView.dequeueReusableCell(withIdentifier: AttributeCell.identifier, for: indexPath) as? AttributeCell {
                cell.backgroundColor = UIColor.white
                let proveedores_array = item.proveedor as NSArray
                let dict_proveedor = proveedores_array[indexPath.row] as! NSDictionary
                cell.lbl_name.text = dict_proveedor["supplier_shortname"] as? String
                var payment_amount_double : Double = 0
                if(dict_proveedor["amount_to_pay"] != nil)
                {
                    payment_amount_double = Double(dict_proveedor["amount_to_pay"] as! String)!
                    cell.lbl_amount.text = convertDoubleToCurrency(amount: payment_amount_double)
                }
                else
                {
                    cell.lbl_amount.text = ""
                }


    
            return cell
            }
        case .cpp:
            if let item = item as? ProfileViewModeCPPItem, let cell = tableView.dequeueReusableCell(withIdentifier: AttributeCell.identifier, for: indexPath) as? AttributeCell {
                //cell.item = item.cpp[indexPath.row]
            return cell
            }
        }
        
        return UITableViewCell()
    }
}

extension VCGViewModel: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if(items.count==0)
        {
            return UIView()
        }
        if let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: HeaderView.identifier) as? HeaderView {
            let item = items[section]
            
            if section % 2 == 0
            {
                headerView.contentView.backgroundColor = UIColor(red: 153/255.0, green: 135/255.0, blue: 157/255.0, alpha: 1.0).withAlphaComponent(0.1)
            }

            //headerView.backgroundColor = UIColor.white
            headerView.item = item
            headerView.section = section
            headerView.delegate = self
            return headerView
        }
        return UIView()
    }
}

extension VCGViewModel: HeaderViewDelegate {
    func toggleSection(header: HeaderView, section: Int) {
        var item = items[section]
        if item.isCollapsible {
            
            // Toggle collapse
            let collapsed = !item.isCollapsed
            item.isCollapsed = collapsed
            header.setCollapsed(collapsed: collapsed)
            
            // Adjust the number of the rows inside the section
            reloadSections?(section)
        }
    }
}
class ProfileViewModeClientesItem: VCGViewModelItem {

    var type: VCGViewModelItemType {
        return .clientes
    }
    
    var sectionTitle: [NSDictionary] {
        return section
    }
    
    var rowCount: Int {
        return clientes.count
    }
    
    var isCollapsed = true
    
    var clientes: [NSMutableArray]
    var section: [NSDictionary]
    
    init(clientes: [NSMutableArray],section:[NSDictionary]) {
        self.clientes = clientes
        self.section = section
    }
}

class ProfileViewModeVentasItem: VCGViewModelItem {

    var type: VCGViewModelItemType {
        return .ventas
    }
    
    var sectionTitle:  [NSDictionary] {
        return section
    }
    
    var rowCount: Int {
        return ventas.count
    }
    
    var isCollapsed = true
    
    var ventas: [NSMutableArray]
    var section: [NSDictionary]
    
    init(ventas: [NSMutableArray],section:[NSDictionary]) {
        self.ventas = ventas
        self.section = section
    }
}


class ProfileViewModeCPCItem: VCGViewModelItem {

    var type: VCGViewModelItemType {
        return .cpc
    }
    
    var sectionTitle: [NSDictionary] {
        return section
    }
    
    var rowCount: Int {
        return cpc.count
    }
    
    var isCollapsed = true
    
    var cpc: [NSMutableArray]
    var section: [NSDictionary]
    
    
    init(cpc: [NSMutableArray],section:[NSDictionary]) {
        self.cpc = cpc
        self.section = section
    }
}

class ProfileViewModeComprasItem: VCGViewModelItem {

    var type: VCGViewModelItemType {
        return .compras
    }
    
    var sectionTitle: [NSDictionary] {
        return section
    }
    
    var rowCount: Int {
        return compras.count
    }
    
    var isCollapsed = true
    
    var compras: [NSMutableArray]
    var section: [NSDictionary]
    
    init(compras: [NSMutableArray],section:[NSDictionary]) {
        self.compras = compras
        self.section = section
    }
}

class ProfileViewModeGastosItem: VCGViewModelItem {

    var type: VCGViewModelItemType {
        return .gastos
    }
    
    var sectionTitle: [NSDictionary] {
        return section
    }
    
    var rowCount: Int {
        return gastos.count
    }
    
    var isCollapsed = true
    
    var gastos: [NSMutableArray]
    var section: [NSDictionary]
    
    init(gastos: [NSMutableArray],section:[NSDictionary]) {
        self.gastos = gastos
        self.section = section
    }
}

class ProfileViewModeProveedorItem: VCGViewModelItem {

    var type: VCGViewModelItemType {
        return .proveedor
    }
    
    var sectionTitle: [NSDictionary] {
        return section
    }
    
    var rowCount: Int {
        return proveedor.count
    }
    
    var isCollapsed = true
    
    var proveedor: [NSMutableArray]
    var section: [NSDictionary]
    
    init(proveedor: [NSMutableArray],section:[NSDictionary]) {
        self.proveedor = proveedor
         self.section = section
    }
}

class ProfileViewModeCPPItem: VCGViewModelItem {

    var type: VCGViewModelItemType {
        return .cpp
    }
    
    var sectionTitle:[NSDictionary] {
        return section
    }
    
    var rowCount: Int {
        return cpp.count
    }
    
    var isCollapsed = true
    
    var cpp: [NSMutableArray]
    var section: [NSDictionary]
    
    init(cpp: [NSMutableArray],section:[NSDictionary]) {
        self.cpp = cpp
         self.section = section
    }
}
