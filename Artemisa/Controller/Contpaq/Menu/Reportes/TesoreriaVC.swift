//
//  TesoreriaVC.swift
//  Artemisa
//
//  Created by Arturo Escamilla on 16/04/20.
//  Copyright © 2020 Microquasar. All rights reserved.
//

import UIKit
import SwiftSpinner

class CustomTViewCell: UITableViewCell
{
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_amount: UILabel!
    @IBOutlet weak var lbl_type: UILabel!
    
    @IBOutlet weak var lbl_date: UILabel!
}


class TesoreriaVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    
    @IBOutlet weak var view_banks: UIStackView!
    
    @IBOutlet weak var sv_tesoreria: UIScrollView!
    
    @IBOutlet weak var lbl_company_name: UILabel!
    
    //Seccion Saldos
    var bank_array  = NSArray()
    
    @IBOutlet weak var view_bancos: UIView!
    @IBOutlet weak var view_suppliers: UIStackView!
    @IBOutlet weak var view_clients: UIStackView!
    
    
    @IBOutlet weak var lbl_saldo_bancos: UILabel!
    @IBOutlet weak var view_tv_bancos: UIView!
    @IBOutlet weak var tv_saldo_bancos: UITableView!
    @IBOutlet weak var btn_mostrar_saldos_detalle: UILabel!
    
    
    //Seccion Ingresos y egresos
    
    @IBOutlet weak var view_tv_in_eg: UIView!
    @IBOutlet weak var tv_in_eg: UITableView!
    @IBOutlet weak var btn_mostrar_ingresos_detalles: UILabel!
    var movimientos_array  = NSMutableArray()
    var sortedArray = NSArray()
    
    //Seccion Proveedores
    
    @IBOutlet weak var view_proveedores: UIView!
    @IBOutlet weak var lbl_total_proveedores: UILabel!
    var proveedores_array  = NSMutableArray()
    @IBOutlet weak var view_tv_proveedores: UIView!
    @IBOutlet weak var tv_proveedores: UITableView!
    
    //Seccion Clientes
    
    @IBOutlet weak var view_clientes: UIView!
    @IBOutlet weak var lbl_total_clientes: UILabel!
    var clientes_array  = NSMutableArray()
    @IBOutlet weak var view_tv_clientes: UIView!
    
    @IBOutlet weak var tv_clientes: UITableView!
    
    
    //Seccion Provisiones
    
    @IBOutlet weak var view_provisiones: UIView!
    @IBOutlet weak var view_tv_provisiones: UIView!
    @IBOutlet weak var lbl_total_provisiones: UILabel!
    var provisiones_array  = NSMutableArray()
    @IBOutlet weak var tv_provisiones: UITableView!
    
    
    let invokeServ = InvokeService()
    let alertmanager = AlertManager()
    var comp_id = 0
    var engrane_name = ""
    let mxLocale = Locale(identifier: "es_MX")
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sv_tesoreria.delegate = self
        sv_tesoreria.isDirectionalLockEnabled = true
        
        let saved_company = GlobalMembers.preferences.dictionary(forKey: GlobalMembers.companyKey)
        if(saved_company != nil)
        {
            let comp_shortname = saved_company!["short_name"] as! String
            comp_id = saved_company!["id"] as! Int
            engrane_name =  saved_company!["engrane_name"] as! String
            lbl_company_name.text = comp_shortname

            let with_contpaq = saved_company!["with_contpaq"] as! Bool
//            if(!with_contpaq)
//            {
                view_clients.isHidden = true
                view_suppliers.isHidden = true
//            }
//            else
//            {
//                view_clients.isHidden = false
//                view_suppliers.isHidden = false
//            }
            
        }
        
        init_view();
        
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
           super.viewDidAppear(true)
        
       getBankInfo();
       getClientsandSuppliersInfo()
      

    }
    
    func init_view()
    {
        //Bancos
        self.view_bancos.backgroundColor = UIColor(red: 116/255.0, green: 116/255.0, blue: 128/255.0, alpha: 1.0).withAlphaComponent(0.08)
        self.view_tv_bancos.backgroundColor = UIColor(red: 116/255.0, green: 116/255.0, blue: 128/255.0, alpha: 1.0).withAlphaComponent(0.08)
        view_tv_bancos.layer.cornerRadius = 12;
        view_tv_bancos.layer.masksToBounds = true;
        tv_saldo_bancos.backgroundView?.backgroundColor = UIColor.white
        tv_saldo_bancos.backgroundColor = UIColor.white
        self.lbl_saldo_bancos.text = "0.00"
        
      
        
        self.view_tv_in_eg.backgroundColor =  UIColor(red: 116/255.0, green: 116/255.0, blue: 128/255.0, alpha: 1.0).withAlphaComponent(0.08)
        view_tv_in_eg.layer.cornerRadius = 12;
        view_tv_in_eg.layer.masksToBounds = true;
        tv_in_eg.backgroundView?.backgroundColor = UIColor.white
        tv_in_eg.backgroundColor = UIColor.white
        
        self.view_proveedores.backgroundColor = UIColor(red: 116/255.0, green: 116/255.0, blue: 128/255.0, alpha: 1.0).withAlphaComponent(0.08)
        self.view_tv_proveedores.backgroundColor = UIColor(red: 116/255.0, green: 116/255.0, blue: 128/255.0, alpha: 1.0).withAlphaComponent(0.08)
        view_tv_proveedores.layer.cornerRadius = 12;
        view_tv_proveedores.layer.masksToBounds = true;
        self.tv_proveedores.backgroundView?.backgroundColor = UIColor.white
        self.tv_proveedores.backgroundColor = UIColor.white
        
        
        self.view_clientes.backgroundColor = UIColor(red: 116/255.0, green: 116/255.0, blue: 128/255.0, alpha: 1.0).withAlphaComponent(0.08)
        self.view_tv_clientes.backgroundColor = UIColor(red: 116/255.0, green: 116/255.0, blue: 128/255.0, alpha: 1.0).withAlphaComponent(0.08)
        view_tv_clientes.layer.cornerRadius = 12;
        view_tv_clientes.layer.masksToBounds = true;
        self.tv_clientes.backgroundView?.backgroundColor = UIColor.white
        self.tv_clientes.backgroundColor = UIColor.white
        
        self.view_provisiones.backgroundColor = UIColor(red: 116/255.0, green: 116/255.0, blue: 128/255.0, alpha: 1.0).withAlphaComponent(0.08)
        self.view_tv_provisiones.backgroundColor = UIColor(red: 116/255.0, green: 116/255.0, blue: 128/255.0, alpha: 1.0).withAlphaComponent(0.08)
        view_tv_provisiones.layer.cornerRadius = 12;
        view_tv_provisiones.layer.masksToBounds = true;
        self.tv_provisiones.backgroundView?.backgroundColor = UIColor.white
        self.tv_provisiones.backgroundColor = UIColor.white
        
        
        
        
        
    }
    
    // Get Info bancos
    func getBankInfo(){

                let response = GlobalMembers.preferences.dictionary(forKey: GlobalMembers.bankinfoKey)
                if(response == nil)
                {
                    view_banks.isHidden = true
                    return
                }

                self.bank_array  = NSArray()

                let bank_response = response!["bancos"] as! NSArray

                self.bank_array = bank_response
                
                self.movimientos_array = NSMutableArray()
                self.sortedArray = NSArray()
                
                for dic in self.bank_array {
                    let bank = dic as! NSDictionary
                    let mov = bank["movimientos"] as! NSArray
                    for item in mov {
                        let i = item as! NSDictionary
                        let egreso = i["egresos"] as! Double
                        let ingreso = i["ingresos"] as! Double
                        let concepto = i["conceptos"] as! String
                        let date = i["date"] as! String
                        
                        if(concepto != "SALDO INICIAL")
                        {
                            if(egreso > 0)
                            {
                                let dictParams: NSMutableDictionary? = ["bank" : bank["bank"] as! String,
                                                                        "amount" :egreso, "concepto" : concepto,
                                                                        "tipo":"egreso","date" : date]
                                self.movimientos_array.add(dictParams!)
                            }
                            if(ingreso > 0)
                            {
                                let dictParams: NSMutableDictionary? = ["bank" : bank["bank"] as! String,
                                                                        "amount" :ingreso, "concepto" : concepto, "tipo" : "ingreso","date" : date ]
                                self.movimientos_array.add(dictParams!)
                            }
                        }
                        
                    }
                }
        
                let df = DateFormatter()
                df.dateFormat = "yyyy-MM-dd"
       
                sortedArray = (movimientos_array as NSArray).sortedArray(using: [NSSortDescriptor(key: "date", ascending: false)]) as! [[String:AnyObject]] as NSArray       
               
        

                self.tv_in_eg.reloadData()
                self.tv_saldo_bancos.reloadData()

                let saldo_total_bancos = response!["saldo_al_momento_bancos"] as! Double
                self.lbl_saldo_bancos.text = self.convertDoubleToCurrency(amount: saldo_total_bancos)
                

            

        

    }
    
    // Get Info CXC y CXP
    func getClientsandSuppliersInfo(){
        
        let res_cxc = GlobalMembers.preferences.array(forKey: GlobalMembers.cxcinfoKey) ?? [Any]()
        //var res_cxc = GlobalMembers.preferences.array(forKey: GlobalMembers.cxcinfoKey)!

                self.clientes_array = NSMutableArray()
                self.proveedores_array = NSMutableArray()
                
                //let res_cxc = response["cxc_total_amount"] as! NSMutableArray
              

                var cxc_total = 0.0
                
                for dic in res_cxc {
                    let comp = dic as! NSDictionary
                    let pay = comp["total"] as! Double
                    if(pay > 0)
                    {
                        self.clientes_array.add(dic)
                    }
                    cxc_total = cxc_total + pay
                }
                
               let res_cxp = GlobalMembers.preferences.array(forKey: GlobalMembers.cxpinfoKey) ?? [Any]()
        
                
                var cxp_total = 0.0
                
                for dic in res_cxp {
                    let comp = dic as! NSDictionary
                    let pay = comp["total"] as! Double
                    if(pay > 0)
                    {
                        self.proveedores_array.add(dic)
                    }
                    cxp_total = cxp_total + pay
                }
                
                DispatchQueue.main.async
                {
                    self.tv_clientes.reloadData()
                    self.tv_proveedores.reloadData()
                    SwiftSpinner.hide()
                    self.lbl_total_clientes.text = self.convertDoubleToCurrency(amount: cxc_total)
                    self.lbl_total_proveedores.text = self.convertDoubleToCurrency(amount: cxp_total)
                }

            

        

    }
//
//    // Get Info Proveedores
//    func getProveedoresInfo(){
//        SwiftSpinner.show("Cargando...")
//
//        invokeServ.requestCXPByCompanyDataWith(company_id: comp_id) { (response, error) in
//
//            if error == nil
//            {
//                self.proveedores_array  = NSMutableArray()
//
//                let proveedor_response = response!["bi_cxp_sumary_by_company_and_supplier"] as! NSArray
//
//
//                var payment_amount_double_total : Double = 0
//
//                var i = 0
//                for proveedor in proveedor_response
//                 {
//                        let prov_dict = proveedor as! NSDictionary
//
//                        let payment_amount = prov_dict["payment_amount"] as! String
//                        let payment_amount_double = Double(payment_amount)
//                        payment_amount_double_total = payment_amount_double_total + payment_amount_double!
//                        if(i <= 4)
//                        {
//                            self.proveedores_array.add(proveedor)
//                            i = i + 1
//                        }
//
//                }
//                DispatchQueue.main.async
//                {
//                    self.tv_proveedores.reloadData()
//                    SwiftSpinner.hide()
//                    self.lbl_total_proveedores.text = self.convertDoubleToCurrency(amount: payment_amount_double_total)
//                }
//
//            }
//
//        }
//
//    }
//
//
//    // Get Info Clientes
//    func getClientesInfo(){
//        SwiftSpinner.show("Cargando...")
//
//        invokeServ.requestCXCByCompanyDataWith(company_id: comp_id) { (response, error) in
//
//            if error == nil
//            {
//                self.clientes_array  = NSMutableArray()
//
//                let clientes_response = response!["bi_cxc_sumary_by_company_and_customer"] as! NSArray
//
//
//                var receivable_amount_double_total : Double = 0
//
//                var i = 0
//                for clientes in clientes_response
//                 {
//                        let cli_dict = clientes as! NSDictionary
//
//                        let receivable_amount = cli_dict["receivable_amount"] as! String
//                        let receivable_amount_double = Double(receivable_amount)
//                        receivable_amount_double_total = receivable_amount_double_total + receivable_amount_double!
//                        if(i <= 4)
//                        {
//                            self.clientes_array.add(clientes)
//                            i = i + 1
//                        }
//
//                }
//                DispatchQueue.main.async
//                {
//                    self.tv_clientes.reloadData()
//                    SwiftSpinner.hide()
//                    self.lbl_total_clientes.text = self.convertDoubleToCurrency(amount: receivable_amount_double_total)
//                }
//
//            }
//
//        }
//
//    }
//
//    // Get Info Provisiones
//    func getProvisionesInfo(){
//
//        SwiftSpinner.show("Cargando...")
//
//        invokeServ.requestProvisionDataWith(company_id: comp_id) { (response, error) in
//
//            if error == nil
//            {
//                self.provisiones_array  = NSMutableArray()
//
//                let provisiones_response = response!["bi_cxp_sumary_payment_provision"] as! NSArray
//
//
//                var payment_provision_amount_double_total : Double = 0
//
//                var i = 0
//                for provision in provisiones_response
//                 {
//                        let prov_dict = provision as! NSDictionary
//
//                        let payment_provision_amount = prov_dict["payment_provision_amount"] as! String
//                        let payment_provision_amount_double = Double(payment_provision_amount)
//                        payment_provision_amount_double_total = payment_provision_amount_double_total + payment_provision_amount_double!
//                        if(i <= 5)
//                        {
//                            self.provisiones_array.add(provision)
//                            i = i + 1
//                        }
//
//                }
//                DispatchQueue.main.async
//                {
//                    self.tv_provisiones.reloadData()
//                    SwiftSpinner.hide()
//                    self.lbl_total_provisiones.text = self.convertDoubleToCurrency(amount: payment_provision_amount_double_total)
//                }
//
//            }
//
//        }
//
//    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.white
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch tableView {
        case tv_saldo_bancos:
             return bank_array.count
        case tv_in_eg:
            return sortedArray.count
        case tv_proveedores:
            return proveedores_array.count
        case tv_clientes:
            return clientes_array.count
        case tv_provisiones:
            return provisiones_array.count
        default:
            break
        }
           
        return 0
    }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_custom", for: indexPath) as! CustomTViewCell
        cell.backgroundColor = UIColor.white
        switch tableView {
        case tv_saldo_bancos:
            let bank_dic = bank_array[indexPath.row] as! NSDictionary
            cell.lbl_name.text = (bank_dic["bank"] as! String)
            let bank_amount = bank_dic["saldo_actual"] as! Double
            cell.lbl_amount.text = convertDoubleToCurrency(amount: bank_amount)
            cell.contentView.backgroundColor = UIColor(red: 116/255.0, green: 116/255.0, blue: 128/255.0, alpha: 1.0).withAlphaComponent(0.08)
            break
        case tv_in_eg:
            let mov_dic = sortedArray[indexPath.row] as! NSDictionary
            cell.lbl_name.text = (mov_dic["concepto"] as! String)
            let bank_amount = mov_dic["amount"] as! Double
            cell.lbl_amount.text = convertDoubleToCurrency(amount: bank_amount)
            cell.lbl_date.text = mov_dic["date"] as! String
            cell.lbl_type.isHidden = false
            cell.lbl_type.text = (mov_dic["tipo"] as! String).capitalized + " (" + (mov_dic["bank"] as! String) + ")"
            cell.contentView.backgroundColor = UIColor(red: 116/255.0, green: 116/255.0, blue: 128/255.0, alpha: 1.0).withAlphaComponent(0.08)
            break
        case tv_proveedores:
            let prov_dic = proveedores_array[indexPath.row] as! NSDictionary
            cell.lbl_name.text = (prov_dic["CRAZONSOCIAL"] as! String)
            let prov_amount = prov_dic["total"] as! Double
            cell.lbl_amount.text = convertDoubleToCurrency(amount: prov_amount)
            cell.contentView.backgroundColor = UIColor(red: 116/255.0, green: 116/255.0, blue: 128/255.0, alpha: 1.0).withAlphaComponent(0.08)
            break
        case tv_clientes:
            let cli_dic = clientes_array[indexPath.row] as! NSDictionary
            cell.lbl_name.text = (cli_dic["CRAZONSOCIAL"] as! String)
            let cli_amount = cli_dic["total"] as! Double
            cell.lbl_amount.text = convertDoubleToCurrency(amount: cli_amount)
            cell.contentView.backgroundColor = UIColor(red: 116/255.0, green: 116/255.0, blue: 128/255.0, alpha: 1.0).withAlphaComponent(0.08)
            break
            
        case tv_provisiones:
            let prov_dic = provisiones_array[indexPath.row] as! NSDictionary
            cell.lbl_name.text = (prov_dic["provision_name"] as! String)
            let prov_amount = prov_dic["payment_provision_amount"] as! String
            let prov_amount_double = Double(prov_amount)
            cell.lbl_amount.text = convertDoubleToCurrency(amount: prov_amount_double!)
            cell.contentView.backgroundColor = UIColor(red: 116/255.0, green: 116/255.0, blue: 128/255.0, alpha: 1.0).withAlphaComponent(0.08)
            break
        
        default:
            break
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(tableView == tv_in_eg)
        {
            return 80
        }
        return 50
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func back_action(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
    private func scrollViewDidScroll(scrollView: UIScrollView) {
        if scrollView.contentOffset.x>0 {
            scrollView.contentOffset.x = 0
        }
    }
}
