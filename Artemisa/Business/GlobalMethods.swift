//
//  GlobalMethods.swift
//  Artemisa
//
//  Created by Arturo Escamilla on 22/04/20.
//  Copyright © 2020 Microquasar. All rights reserved.
//

import UIKit

extension Date {
    var month: String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "es")
        dateFormatter.dateFormat = "MMMM"
        return dateFormatter.string(from: self)
    }
}
extension UIViewController {
    func convertDoubleToCurrency(amount: Double) -> String{
           let numberFormatter = NumberFormatter()
           numberFormatter.numberStyle = .currency
           numberFormatter.locale = Locale.current
           return numberFormatter.string(from: NSNumber(value: amount))!
    }
    func convertCurrencyToDouble(input: String) -> Double? {
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = .currency
            numberFormatter.locale = Locale.current
            return numberFormatter.number(from: input)?.doubleValue
    }
    
    
    
    
}
extension VCGViewModel {
    func convertDoubleToCurrency(amount: Double) -> String{
           let numberFormatter = NumberFormatter()
           numberFormatter.numberStyle = .currency
           numberFormatter.locale = Locale.current
           return numberFormatter.string(from: NSNumber(value: amount))!
    }
    func convertCurrencyToDouble(input: String) -> Double? {
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = .currency
            numberFormatter.locale = Locale.current
            return numberFormatter.number(from: input)?.doubleValue
    }
}
extension HeaderView {
    func convertDoubleToCurrency(amount: Double) -> String{
           let numberFormatter = NumberFormatter()
           numberFormatter.numberStyle = .currency
           numberFormatter.locale = Locale.current
           return numberFormatter.string(from: NSNumber(value: amount))!
    }
    func convertCurrencyToDouble(input: String) -> Double? {
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = .currency
            numberFormatter.locale = Locale.current
            return numberFormatter.number(from: input)?.doubleValue
    }
}


