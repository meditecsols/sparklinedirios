//
//  GlobalMembers.swift
//  Artemisa
//
//  Created by Arturo Escamilla on 31/03/20.
//  Copyright © 2020 Microquasar. All rights reserved.
//

import UIKit

struct GlobalMembers {
    

    static var preferences = UserDefaults.standard
    static var tokenKey = "currentToken"
    static var basicTokenKey = "basicToken"
    static var emailKey = "emailKey"
    static var dateKey = "dateKey"
    static var passwordKey = "passwordKey"
    static var companyKey = "company"
    static var bankinfoKey = "bankinfo"
    static var reportinfoKey = "reportinfo"
    static var cxcinfoKey = "cxcinfoKey"
    static var cxpinfoKey = "cxpinfoKey"
    static var consolidadoKey = "consolidadoKey"
    static var dateConsolidadoKey = "dateConsolidadoKey"
    static var newCompany = false
    
    
    
    //Login Artemisa
    static var tokenArtemisaKey = "currentTokenArtemisa"
    static var emailArtemisaKey = "emailArtemisa"
    static var companyArtemisaKey = "companyArtemisa"
    static var tokenNotifArtemisa = ""
    
}
