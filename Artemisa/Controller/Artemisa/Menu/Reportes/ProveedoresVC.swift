//
//  ProveedoresVC.swift
//  Artemisa
//
//  Created by Arturo Escamilla on 14/04/20.
//  Copyright © 2020 Microquasar. All rights reserved.
//

import UIKit
import SwiftSpinner


class ProveedoresTViewCell: UITableViewCell
{
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_amount: UILabel!
    
}

class ProveedoresVC: UIViewController,UITableViewDelegate, UITableViewDataSource {
   
    
    @IBOutlet weak var tv_proveedores: UITableView!
    
    
    var proveedores_array = NSArray()
    
    let invokeServ = InvokeService()
    let alertmanager = AlertManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SwiftSpinner.show("Cargando...")
        
        self.tv_proveedores.isHidden = true
        
        let saved_company = GlobalMembers.preferences.dictionary(forKey: GlobalMembers.companyKey)
        if(saved_company != nil)
        {
            let comp_id = saved_company!["company_id"] as! String
            
            invokeServ.requestCXPByCompanyDataWith(company_id: comp_id)
            { (response, error) in
                if error == nil
                {

                    self.proveedores_array = response!["bi_cxp_sumary_by_company_and_supplier"] as! NSArray
                    DispatchQueue.main.async
                    {
                        SwiftSpinner.hide()
                        self.tv_proveedores.isHidden = false
                        self.tv_proveedores.reloadData()
                    }
                }

            }
        }
        // Do any additional setup after loading the view.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return proveedores_array.count
    }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell_proveedores", for: indexPath) as!   ProveedoresTViewCell
            let proveedores_dict = proveedores_array[indexPath.row] as! NSDictionary
            
            cell.lbl_name.text = proveedores_dict["supplier_shortname"] as? String
            let payment_amount = proveedores_dict["payment_amount"] as! String
            let payment_amount_double = Double(payment_amount)
        
            cell.lbl_amount.text = convertDoubleToCurrency(amount: payment_amount_double!)
        
            return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return 86
    }

    @IBAction func back_action(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
