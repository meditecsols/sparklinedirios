//
//  LoginVC.swift
//  Artemisa
//
//  Created by Arturo Escamilla on 18/02/20.
//  Copyright © 2020 Microquasar. All rights reserved.
//

import UIKit
import SwiftSpinner

let invokeServ = InvokeService()
let alertmanager = AlertManager()


class LoginVC: UIViewController {
    
    @IBOutlet weak var txt_user: UITextField!
    @IBOutlet weak var txt_password: UITextField!
    
    @IBAction func back_action(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
      
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))

        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false

        view.addGestureRecognizer(tap)
        
        observeKeyboardNotifications()
        
        txt_user.attributedPlaceholder = NSAttributedString(string:"Usuario", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        txt_password.attributedPlaceholder = NSAttributedString(string:"Contraseña", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
        
        
        if(token != nil)
        {
            self.performSegue(withIdentifier: "segue_menu", sender: nil)
        }

        
        
    }

    //Calls this function when the tap is recognized.
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    fileprivate func observeKeyboardNotifications()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardShow), name:UIResponder.keyboardWillShowNotification , object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide), name:UIResponder.keyboardWillHideNotification , object: nil)
        
    }
    @objc func keyboardHide()
    {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.view.frame = CGRect(x: 0, y:0, width: self.view.frame.width, height: self.view.frame.height)
        }, completion: nil)
        
    }

    @objc func keyboardShow()
    {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.view.frame = CGRect(x: 0, y: -150, width: self.view.frame.width, height: self.view.frame.height)
        }, completion: nil)
    
    }
    
    
    @IBAction func enter_action(_ sender: Any) {
        
        if(txt_user.text?.count == 0)
        {
            alertmanager.alert(title: "Sparkline", mess: "Ingresa un correo", viewcontroller: self)
            return
        }
        else
        {
            if !isValidEmail(emailStr: txt_user.text!)
            {
                alertmanager.alert(title: "Sparkline",mess: "Ingresa un correo valido" , viewcontroller: self);
                return
            }
        }
        
        if txt_password.text?.count == 0
        {
            alertmanager.alert(title: "Sparkline",mess: "Ingresa una contraseña" , viewcontroller: self);
            return;
        }
        self.keyboardHide()
        SwiftSpinner.show("Entrando...")
        let user = txt_user.text!
        let password = txt_password.text!
        let dictParams: NSMutableDictionary? = ["username" : user,
                                                       "password" : password]
        
        invokeServ.requestForLoginDataWith(dictParams!) { (result, error) in
            
                if result != nil
                {
                    
                    if let val = result!["error"] {
                        DispatchQueue.main.async
                        {
                            SwiftSpinner.hide()
                            alertmanager.alert(title: "Sparkline",mess: "Revisa tu usuario o contraseña" , viewcontroller: self);
                            return;
                        }
                       
                    }
                    else
                    {
                    
                    
                     let dictParamsToken: NSMutableDictionary? = ["grant_type":"password" as Any, "username" : user, "password" : password]
                    let clientid = result!["client_id"] as! String
                            let clientsecret = result!["client_secret"] as! String
                    
                    let longstring = clientid + ":" + clientsecret
                    let data = (longstring).data(using: String.Encoding.utf8)
                    let basic_token = data!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
                    
                    invokeServ.requestForTokenDataWith(dictParamsToken as! [NSMutableDictionary : NSMutableDictionary], token: basic_token) { (result, error) in
                        
                            if result != nil
                            {
                              
                                //let dictaccesstoken: NSMutableDictionary? = ["access_token" : result!["access_token"] as Any , "token_type" : result!["token_type"] as Any]
                                let accesstoken = result!["access_token"] as! String
                                
                                let token = "Bearer " + accesstoken
                                
                                GlobalMembers.preferences.set(token, forKey: GlobalMembers.tokenKey)
                                GlobalMembers.preferences.set(basic_token, forKey: GlobalMembers.basicTokenKey)
                                GlobalMembers.preferences.set(user, forKey: GlobalMembers.emailKey)
                                GlobalMembers.preferences.set(password, forKey: GlobalMembers.passwordKey)
                                
                                let didSave = GlobalMembers.preferences.synchronize()

                                if didSave {
                                    
                                    if GlobalMembers.preferences.object(forKey: GlobalMembers.tokenKey) != nil {

                                        GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
                                        
                                            DispatchQueue.main.async
                                            {
                                                SwiftSpinner.hide()
                                                 //self.dismiss(animated: true, completion: nil)
                                                self.performSegue(withIdentifier: "segue_menu", sender: nil)
                                               
                                            }

                                            
                                        }
                                   }
                                    
                                
                            }
                            else
                            {
                                
                               
                                    DispatchQueue.main.async
                                    {
                                        SwiftSpinner.hide()
                                        alertmanager.alert(title: "Sparkline",mess: "Revisa tu usuario o contraseña" , viewcontroller: self);
                                    }
                               
                            }
                    
                        }
                    }
                        
                    
                }
            
            
            
            
            
        
            }
        
        
    }
    
    func isValidEmail(emailStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
    }
    

    

}
