//
//  VcgVC.swift
//  Artemisa
//
//  Created by Arturo Escamilla on 31/03/20.
//  Copyright © 2020 Microquasar. All rights reserved.
//

import UIKit

//class VCGTViewCell: UITableViewCell
//{
//    
//    @IBOutlet weak var lbl_title: UILabel!
//    
//    @IBOutlet weak var lbl_total: UILabel!
//}

class VcgVC: UIViewController{
    
    let invokeServ = InvokeService()
    let alertmanager = AlertManager()
    
    fileprivate let viewModel = VCGViewModel()
    
    
    
    @IBOutlet weak var lbl_company_name: UILabel!
    @IBOutlet weak var tv_vcg: UITableView!
    var menu_array = NSMutableArray()
    let mxLocale = Locale(identifier: "es_MX")
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let saved_company = GlobalMembers.preferences.dictionary(forKey: GlobalMembers.companyArtemisaKey)
        if(saved_company != nil)
        {
            let comp_shortname = saved_company!["company_shortname"] as! String
            lbl_company_name.text = comp_shortname

            
        }
        
        viewModel.reloadSections = { [weak self] (section: Int) in
            self?.tv_vcg?.beginUpdates()
            self?.tv_vcg?.reloadSections([section], with: .fade)
            self?.tv_vcg?.endUpdates()
        }
        tv_vcg?.backgroundColor = UIColor.white
        //tv_vcg?.backgroundView?.backgroundColor = UIColor.white
        tv_vcg?.estimatedRowHeight = 80
        tv_vcg?.rowHeight = UITableView.automaticDimension
        tv_vcg?.sectionHeaderHeight = 90
        tv_vcg?.separatorStyle = .none
        tv_vcg?.dataSource = viewModel
        tv_vcg?.delegate = viewModel
        tv_vcg?.register(AttributeCell.nib, forCellReuseIdentifier: AttributeCell.identifier)
        tv_vcg?.register(HeaderView.nib, forHeaderFooterViewReuseIdentifier: HeaderView.identifier)
        
        
        
        

        // Do any additional setup after loading the view.
    }
    
    @IBAction func back_action(_ sender: Any) {
        
        
         dismiss(animated: true, completion: nil)
    }

    


}
