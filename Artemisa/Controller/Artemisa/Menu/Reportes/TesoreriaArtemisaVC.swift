//
//  TesoreriaArtemisaVC.swift
//  Artemisa
//
//  Created by Arturo Escamilla on 15/07/20.
//  Copyright © 2020 Microquasar. All rights reserved.
//

import UIKit

import SwiftSpinner

class CustomArtemisaTViewCell: UITableViewCell
{
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_amount: UILabel!
    
}

class TesoreriaArtemisaVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var sv_tesoreria: UIScrollView!
    
    @IBOutlet weak var lbl_company_name: UILabel!
    
    //Seccion Saldos
    var bank_array  = NSMutableArray()
    
    @IBOutlet weak var view_bancos: UIView!
    @IBOutlet weak var lbl_saldo_bancos: UILabel!
    @IBOutlet weak var view_tv_bancos: UIView!
    @IBOutlet weak var tv_saldo_bancos: UITableView!
    @IBOutlet weak var btn_mostrar_saldos_detalle: UILabel!
    
    
    //Seccion Ingresos y egresos
    
    @IBOutlet weak var view_tv_in_eg: UIView!
    @IBOutlet weak var tv_in_eg: UITableView!
    @IBOutlet weak var btn_mostrar_ingresos_detalles: UILabel!
    
    //Seccion Proveedores
    
    @IBOutlet weak var view_proveedores: UIView!
    @IBOutlet weak var lbl_total_proveedores: UILabel!
    var proveedores_array  = NSMutableArray()
    @IBOutlet weak var view_tv_proveedores: UIView!
    @IBOutlet weak var tv_proveedores: UITableView!
    
    //Seccion Clientes
    
    @IBOutlet weak var view_clientes: UIView!
    @IBOutlet weak var lbl_total_clientes: UILabel!
    var clientes_array  = NSMutableArray()
    @IBOutlet weak var view_tv_clientes: UIView!
    
    @IBOutlet weak var tv_clientes: UITableView!
    
    
    //Seccion Provisiones
    
    @IBOutlet weak var view_provisiones: UIView!
    @IBOutlet weak var view_tv_provisiones: UIView!
    @IBOutlet weak var lbl_total_provisiones: UILabel!
    var provisiones_array  = NSMutableArray()
    @IBOutlet weak var tv_provisiones: UITableView!
    
    
    let invokeServ = InvokeService()
    let alertmanager = AlertManager()
    var comp_id = ""
    let mxLocale = Locale(identifier: "es_MX")
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sv_tesoreria.delegate = self
        sv_tesoreria.isDirectionalLockEnabled = true
        
        let saved_company = GlobalMembers.preferences.dictionary(forKey: GlobalMembers.companyArtemisaKey)
        if(saved_company != nil)
        {
            let comp_shortname = saved_company!["company_shortname"] as! String
            comp_id = saved_company!["company_id"] as! String
            lbl_company_name.text = comp_shortname

            
        }
        
        init_view();
        
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
           super.viewDidAppear(true)
        
        getBankInfo();
        getProveedoresInfo()
        getClientesInfo()
        getProvisionesInfo()
          
    }
    
    func init_view()
    {
        //Bancos
        self.view_bancos.backgroundColor = UIColor(red: 116/255.0, green: 116/255.0, blue: 128/255.0, alpha: 1.0).withAlphaComponent(0.08)
        self.view_tv_bancos.backgroundColor = UIColor(red: 116/255.0, green: 116/255.0, blue: 128/255.0, alpha: 1.0).withAlphaComponent(0.08)
        view_tv_bancos.layer.cornerRadius = 12;
        view_tv_bancos.layer.masksToBounds = true;
        tv_saldo_bancos.backgroundView?.backgroundColor = UIColor.white
        tv_saldo_bancos.backgroundColor = UIColor.white
        self.lbl_saldo_bancos.text = "0.00"
        
      
        
        self.view_tv_in_eg.backgroundColor =  UIColor(red: 116/255.0, green: 116/255.0, blue: 128/255.0, alpha: 1.0).withAlphaComponent(0.08)
        view_tv_in_eg.layer.cornerRadius = 12;
        view_tv_in_eg.layer.masksToBounds = true;
        tv_in_eg.backgroundView?.backgroundColor = UIColor.white
        tv_in_eg.backgroundColor = UIColor.white
        
        self.view_proveedores.backgroundColor = UIColor(red: 116/255.0, green: 116/255.0, blue: 128/255.0, alpha: 1.0).withAlphaComponent(0.08)
        self.view_tv_proveedores.backgroundColor = UIColor(red: 116/255.0, green: 116/255.0, blue: 128/255.0, alpha: 1.0).withAlphaComponent(0.08)
        view_tv_proveedores.layer.cornerRadius = 12;
        view_tv_proveedores.layer.masksToBounds = true;
        self.tv_proveedores.backgroundView?.backgroundColor = UIColor.white
        self.tv_proveedores.backgroundColor = UIColor.white
        
        
        self.view_clientes.backgroundColor = UIColor(red: 116/255.0, green: 116/255.0, blue: 128/255.0, alpha: 1.0).withAlphaComponent(0.08)
        self.view_tv_clientes.backgroundColor = UIColor(red: 116/255.0, green: 116/255.0, blue: 128/255.0, alpha: 1.0).withAlphaComponent(0.08)
        view_tv_clientes.layer.cornerRadius = 12;
        view_tv_clientes.layer.masksToBounds = true;
        self.tv_clientes.backgroundView?.backgroundColor = UIColor.white
        self.tv_clientes.backgroundColor = UIColor.white
        
        self.view_provisiones.backgroundColor = UIColor(red: 116/255.0, green: 116/255.0, blue: 128/255.0, alpha: 1.0).withAlphaComponent(0.08)
        self.view_tv_provisiones.backgroundColor = UIColor(red: 116/255.0, green: 116/255.0, blue: 128/255.0, alpha: 1.0).withAlphaComponent(0.08)
        view_tv_provisiones.layer.cornerRadius = 12;
        view_tv_provisiones.layer.masksToBounds = true;
        self.tv_provisiones.backgroundView?.backgroundColor = UIColor.white
        self.tv_provisiones.backgroundColor = UIColor.white
        
        
        
        
        
    }
    
    // Get Info bancos
    func getBankInfo(){
        
        SwiftSpinner.show("Cargando")
        invokeServ.requestBancosDatabyCompanyWith(company_id: comp_id) { (response, error) in
            
            if error == nil
            {
                self.bank_array  = NSMutableArray()
                
                let bank_response = response!["bi_bks_sumary_currency_balance_by_company_and_bank"] as! NSArray
                
                
                var currency_balance_amount_double_total : Double = 0
                
                var i = 0
                for bank in bank_response
                 {
                        let bank_dict = bank as! NSDictionary
                        
                        let currency_balance_amount = bank_dict["currency_balance_amount"] as! String
                        let currency_balance_amount_double = Double(currency_balance_amount )
                        currency_balance_amount_double_total = currency_balance_amount_double_total + currency_balance_amount_double!
                        if(i <= 4)
                        {
                            self.bank_array.add(bank)
                            i = i + 1
                        }
                        
                }
                DispatchQueue.main.async
                {
                    self.tv_saldo_bancos.reloadData()
                    SwiftSpinner.hide()
                    self.lbl_saldo_bancos.text = self.convertDoubleToCurrency(amount: currency_balance_amount_double_total)
                }
                
            }
            
        }
        
    }
    
    // Get Info Proveedores
    func getProveedoresInfo(){
        SwiftSpinner.show("Cargando...")
        
        invokeServ.requestCXPByCompanyDataWith(company_id: comp_id) { (response, error) in
            
            if error == nil
            {
                self.proveedores_array  = NSMutableArray()
                
                let proveedor_response = response!["bi_cxp_sumary_by_company_and_supplier"] as! NSArray
                
                
                var payment_amount_double_total : Double = 0
                
                var i = 0
                for proveedor in proveedor_response
                 {
                        let prov_dict = proveedor as! NSDictionary
                        
                        let payment_amount = prov_dict["payment_amount"] as! String
                        let payment_amount_double = Double(payment_amount)
                        payment_amount_double_total = payment_amount_double_total + payment_amount_double!
                        if(i <= 4)
                        {
                            self.proveedores_array.add(proveedor)
                            i = i + 1
                        }
                        
                }
                DispatchQueue.main.async
                {
                    self.tv_proveedores.reloadData()
                    SwiftSpinner.hide()
                    self.lbl_total_proveedores.text = self.convertDoubleToCurrency(amount: payment_amount_double_total)
                }
                
            }
            
        }
        
    }
    
    
    // Get Info Clientes
    func getClientesInfo(){
        SwiftSpinner.show("Cargando...")
        
        invokeServ.requestCXCByCompanyDataWith(company_id: comp_id) { (response, error) in
            
            if error == nil
            {
                self.clientes_array  = NSMutableArray()
                
                let clientes_response = response!["bi_cxc_sumary_by_company_and_customer"] as! NSArray
                
                
                var receivable_amount_double_total : Double = 0
                
                var i = 0
                for clientes in clientes_response
                 {
                        let cli_dict = clientes as! NSDictionary
                        
                        let receivable_amount = cli_dict["receivable_amount"] as! String
                        let receivable_amount_double = Double(receivable_amount)
                        receivable_amount_double_total = receivable_amount_double_total + receivable_amount_double!
                        if(i <= 4)
                        {
                            self.clientes_array.add(clientes)
                            i = i + 1
                        }
                        
                }
                DispatchQueue.main.async
                {
                    self.tv_clientes.reloadData()
                    SwiftSpinner.hide()
                    self.lbl_total_clientes.text = self.convertDoubleToCurrency(amount: receivable_amount_double_total)
                }
                
            }
            
        }
        
    }
    
    // Get Info Provisiones
    func getProvisionesInfo(){
        
        SwiftSpinner.show("Cargando...")
        
        invokeServ.requestProvisionDataWith(company_id: comp_id) { (response, error) in
            
            if error == nil
            {
                self.provisiones_array  = NSMutableArray()
                
                let provisiones_response = response!["bi_cxp_sumary_payment_provision"] as! NSArray
                
                
                var payment_provision_amount_double_total : Double = 0
                
                var i = 0
                for provision in provisiones_response
                 {
                        let prov_dict = provision as! NSDictionary
                        
                        let payment_provision_amount = prov_dict["payment_provision_amount"] as! String
                        let payment_provision_amount_double = Double(payment_provision_amount)
                        payment_provision_amount_double_total = payment_provision_amount_double_total + payment_provision_amount_double!
                        if(i <= 5)
                        {
                            self.provisiones_array.add(provision)
                            i = i + 1
                        }
                        
                }
                DispatchQueue.main.async
                {
                    self.tv_provisiones.reloadData()
                    SwiftSpinner.hide()
                    self.lbl_total_provisiones.text = self.convertDoubleToCurrency(amount: payment_provision_amount_double_total)
                }
                
            }
            
        }
        
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.white
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch tableView {
        case tv_saldo_bancos:
             return bank_array.count
        case tv_proveedores:
            return proveedores_array.count
        case tv_clientes:
            return clientes_array.count
        case tv_provisiones:
            return provisiones_array.count
        default:
            break
        }
           
        return 0
    }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_custom", for: indexPath) as! CustomArtemisaTViewCell
        cell.backgroundColor = UIColor.white
        switch tableView {
        case tv_saldo_bancos:
            let bank_dic = bank_array[indexPath.row] as! NSDictionary
            cell.lbl_name.text = (bank_dic["bank_name"] as! String)
            let bank_amount = bank_dic["currency_balance_amount"] as! String
            let bank_amount_double = Double(bank_amount)
            cell.lbl_amount.text = convertDoubleToCurrency(amount: bank_amount_double!)
            cell.contentView.backgroundColor = UIColor(red: 116/255.0, green: 116/255.0, blue: 128/255.0, alpha: 1.0).withAlphaComponent(0.08)
            break
        case tv_proveedores:
            let prov_dic = proveedores_array[indexPath.row] as! NSDictionary
            cell.lbl_name.text = (prov_dic["supplier_shortname"] as! String)
            let prov_amount = prov_dic["payment_amount"] as! String
            let prov_amount_double = Double(prov_amount)
            cell.lbl_amount.text = convertDoubleToCurrency(amount: prov_amount_double!)
            cell.contentView.backgroundColor = UIColor(red: 116/255.0, green: 116/255.0, blue: 128/255.0, alpha: 1.0).withAlphaComponent(0.08)
            break
        case tv_clientes:
            let cli_dic = clientes_array[indexPath.row] as! NSDictionary
            cell.lbl_name.text = (cli_dic["customer_shortname"] as! String)
            let cli_amount = cli_dic["receivable_amount"] as! String
            let cli_amount_double = Double(cli_amount)
            cell.lbl_amount.text = convertDoubleToCurrency(amount: cli_amount_double!)
            cell.contentView.backgroundColor = UIColor(red: 116/255.0, green: 116/255.0, blue: 128/255.0, alpha: 1.0).withAlphaComponent(0.08)
            break
            
        case tv_provisiones:
            let prov_dic = provisiones_array[indexPath.row] as! NSDictionary
            cell.lbl_name.text = (prov_dic["provision_name"] as! String)
            let prov_amount = prov_dic["payment_provision_amount"] as! String
            let prov_amount_double = Double(prov_amount)
            cell.lbl_amount.text = convertDoubleToCurrency(amount: prov_amount_double!)
            cell.contentView.backgroundColor = UIColor(red: 116/255.0, green: 116/255.0, blue: 128/255.0, alpha: 1.0).withAlphaComponent(0.08)
            break
        
        default:
            break
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func back_action(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
    private func scrollViewDidScroll(scrollView: UIScrollView) {
        if scrollView.contentOffset.x>0 {
            scrollView.contentOffset.x = 0
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
