//
//  AlertManager.swift
//  Artemisa
//
//  Created by Arturo Escamilla on 31/03/20.
//  Copyright © 2020 Microquasar. All rights reserved.
//

import UIKit

class AlertManager: NSObject {
    
    func alert(title:String, mess:String, viewcontroller:UIViewController){
        
        let alert = UIAlertController(title: title, message: mess, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
        viewcontroller.self.present(alert, animated: true, completion: nil)
        
    }

}
