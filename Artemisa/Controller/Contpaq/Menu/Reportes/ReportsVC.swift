//
//  ReportsVC.swift
//  Artemisa
//
//  Created by Arturo Escamilla on 06/03/20.
//  Copyright © 2020 Microquasar. All rights reserved.
//

import UIKit
import WebKit
import SwiftSpinner




class ReportesTViewCell: UITableViewCell
{
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_update: UILabel!
}


class ReportsVC: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var view_banks: UIView!
    
    @IBOutlet weak var view_cartera_all: UIView!
    
    @IBOutlet weak var tv_reports: UITableView!
    
    var menu_reports = NSMutableArray()
    
    @IBOutlet weak var lbl_company_name: UILabel!
    
    
    
    @IBOutlet weak var lbl_client_cartera_normal: UILabel!
    @IBOutlet weak var lbl_proveedor_cartera_normal: UILabel!
    
    @IBOutlet weak var lbl_client_cartera_vencida: UILabel!
    @IBOutlet weak var lbl_proveedor_cartera_vencida: UILabel!
    
    @IBOutlet weak var lbl_client_cartera_total: UILabel!
    @IBOutlet weak var lbl_proveedor_cartera_total: UILabel!
    
    @IBOutlet weak var lbl_client_promesa: UILabel!
    @IBOutlet weak var lbl_proveedor_promesa: UILabel!
    
    @IBOutlet weak var lbl_proveedor_provisiones: UILabel!
    
    @IBOutlet weak var lbl_client_saldo_banc: UILabel!
    
    @IBOutlet weak var lbl_client_total: UILabel!
    @IBOutlet weak var lbl_proveedor_total: UILabel!
    
    @IBOutlet weak var view_cartera: UIView!
    @IBOutlet weak var view_total_cartera: UIView!
    
    @IBOutlet weak var view_promesas: UIView!
    @IBOutlet weak var view_promesas_total: UIView!
    
    
    @IBOutlet weak var lbl_date_update: UILabel!
    @IBOutlet weak var sv_report: UIScrollView!
    
    
    let invokeServ = InvokeService()
    let alertmanager = AlertManager()
    
    var company_cxc = NSDictionary()
    var company_cxp = NSDictionary()
    var company_prov = NSMutableArray()
    var company_bank = NSMutableArray()
    
    let mxLocale = Locale(identifier: "es_MX")
    
    var saved_company = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        sv_report.refreshControl = UIRefreshControl()
        sv_report.refreshControl?.addTarget(self, action:
                                             #selector(handleRefreshControl),
                                             for: .valueChanged)
        
        tv_reports.backgroundView?.backgroundColor = UIColor.white
        tv_reports.backgroundColor = UIColor.white
        
        
        // Background View
        
        //self.view_promesas.backgroundColor = UIColor(red: 153/255.0, green: 135/255.0, blue: 157/255.0, alpha: 1.0).withAlphaComponent(0.1)
        self.view_total_cartera.backgroundColor = UIColor(red: 153/255.0, green: 135/255.0, blue: 157/255.0, alpha: 1.0).withAlphaComponent(0.1)
        self.view_promesas_total.backgroundColor = UIColor(red: 153/255.0, green: 135/255.0, blue: 157/255.0, alpha: 1.0).withAlphaComponent(0.1)
     
                

                let opt1 = NSMutableDictionary()
                opt1.setValue("Posición de tesorería", forKey: "name")
                opt1.setValue("Actualizado el 24 de marzo", forKey: "date")
                menu_reports.add(opt1)
                
//                let opt2 = NSMutableDictionary()
//                opt2.setValue("Ventas, compras y gastos", forKey: "name")
//                opt2.setValue("Actualizado el 24 de marzo", forKey: "date")
//                menu_reports.add(opt2)
                
//                let opt3 = NSMutableDictionary()
//                opt3.setValue("Utilidades", forKey: "name")
//                opt3.setValue("Actualizado el 24 de marzo", forKey: "date")
//                menu_reports.add(opt3)
//
//                let opt4 = NSMutableDictionary()
//                opt4.setValue("Gastos", forKey: "name")
//                opt4.setValue("Actualizado el 24 de marzo", forKey: "date")
//                menu_reports.add(opt4)
//
//                let opt5 = NSMutableDictionary()
//                opt5.setValue("Inventarios", forKey: "name")
//                opt5.setValue("Actualizado el 24 de marzo", forKey: "date")
//                menu_reports.add(opt5)
        
        
     
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
           super.viewDidAppear(true)
        
          
            if(!isKeyPresentInUserDefaults(key: GlobalMembers.companyKey))
            {
                self.tabBarController!.selectedIndex = 1
               
                
            }
            else
            {
                saved_company = GlobalMembers.preferences.dictionary(forKey: GlobalMembers.companyKey)! as NSDictionary
          
        
        
                if(saved_company != nil)
                {
                    
                    
                    let comp_shortname = saved_company["short_name"] as! String
                   
                    lbl_company_name.text = comp_shortname
                    
                    if(!isKeyPresentInUserDefaults(key: GlobalMembers.dateKey))
                    {
                        if(GlobalMembers.newCompany)
                        {
                             GlobalMembers.newCompany = false
                             SwiftSpinner.show("Cargando...")
                             getDataBank()
                             GlobalMembers.preferences.set(Date(), forKey: GlobalMembers.dateKey)
                             updatelabeldate()
                        }
                       
                    }
                   else
                    {
                        if(GlobalMembers.newCompany)
                        {
                            GlobalMembers.newCompany = false
                            SwiftSpinner.show("Cargando...")
                            getDataBank()
                            GlobalMembers.preferences.set(Date(), forKey: GlobalMembers.dateKey)
                            updatelabeldate()
                        }
                        else
                        {
//                            let with_contpaq = saved_company["with_contpaq"] as! Bool
//                            if(with_contpaq)
//                            {
//                                fillReportData()
//                            }
                            fillBankData()
                            
                            updatelabeldate()
                        }

                    }
                    
                   
                
                }
                else
                {
                    alertmanager.alert(title: "SparklineSparkline",mess: "Selecciona una empresa" , viewcontroller: self);
                
                }
            }
          
    }
    
    func updatelabeldate()
    {
        let date = GlobalMembers.preferences.object(forKey: GlobalMembers.dateKey) as! Date
        let df = DateFormatter()
        df.dateFormat = "dd-MM-yyyy HH:mm"
        print(df.string(from: date))
        lbl_date_update.text = df.string(from: date)
    }
    
    @objc func handleRefreshControl() {
    
       DispatchQueue.main.async {
          SwiftSpinner.show("Cargando...")
          self.getDataBank()
          GlobalMembers.preferences.set(Date(), forKey: GlobalMembers.dateKey)
          self.updatelabeldate()
          self.sv_report.refreshControl?.endRefreshing()
       }
    }

    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    func getDataBank(){
        let company_engrane = saved_company["engrane_name"] as! String
        
        invokeServ.requestBankataWith(company: company_engrane){ (response, error, code) in
                        
                        if code == 200
                        {
                            
                            if (response?["detail"] != nil)
                            {
                                self.refreshToken()
                                return
                            }
                            
                            
                            DispatchQueue.main.async
                            {
                                self.view_banks.isHidden = false
                                GlobalMembers.preferences.set(response!, forKey: GlobalMembers.bankinfoKey)
                                GlobalMembers.preferences.synchronize()
                                self.fillBankData()
                                SwiftSpinner.hide()
                                //self.getDataReport()
                                
                            }
                            
                        }
                        else
                        {
                            DispatchQueue.main.async
                            { [self] in
                                GlobalMembers.preferences.set(nil, forKey: GlobalMembers.bankinfoKey)
                                GlobalMembers.preferences.synchronize()
                                self.view_banks.isHidden = true
                                self.view_cartera_all.isHidden = true
                                
                                self.tv_reports.isHidden = true
                                //self.getDataReport()
                                self.alertmanager.alert(title: "Sparkline", mess: "Solicita acceso", viewcontroller: self)
                                SwiftSpinner.hide()
                            }
                        }
                        
                        
                    }
        
    }
    func fillBankData()
    {
        let bank_info = GlobalMembers.preferences.dictionary(forKey: GlobalMembers.bankinfoKey)
        if(bank_info == nil)
        {
            view_banks.isHidden = true
            self.view_banks.isHidden = true
            self.view_cartera_all.isHidden = true
            self.tv_reports.isHidden = true
            //self.getDataReport()
            self.alertmanager.alert(title: "Sparkline", mess: "Solicita accesos", viewcontroller: self)
            return
        }
      
        self.view_cartera_all.isHidden = true
        
        let string_saldo = bank_info!["saldo_al_momento_bancos"] as! Double
        let saldo_double = string_saldo
        self.lbl_client_saldo_banc.text = self.convertDoubleToCurrency(amount: saldo_double)
        
    }
    func getDataReport(){
        let company_engrane = saved_company["id"] as! Int
        let with_contpaq = saved_company["with_contpaq"] as! Bool
 //       if(!with_contpaq)
//        {
            view_cartera_all.isHidden = true
//        }
//        else
//        {
//            view_cartera_all.isHidden = false
//        }
        invokeServ.requestReportTotalDatabyCompanyWith(company_id: String(company_engrane)){ (response, error) in
                        
                        if response != nil
                        {
                            
                            if (response?["detail"] != nil)
                            {
                                self.refreshToken()
                                return
                            }
                            
                            
                            DispatchQueue.main.async
                            {
                                
                                GlobalMembers.preferences.set(response!, forKey: GlobalMembers.reportinfoKey)
                                GlobalMembers.preferences.synchronize()
                                self.fillReportData()
                                self.getClientsandSuppliersInfo()
                                //SwiftSpinner.hide()
                                
                            }
                            
                        }
                        else
                        {
                             SwiftSpinner.hide()
                        }
                        
                        
                    }
        
    }
    
    func fillReportData()
    {
        
        let with_contpaq = saved_company["with_contpaq"] as! Bool
//        if(!with_contpaq)
//        {
            view_cartera_all.isHidden = true
//        }
//        else
//        {
//            view_cartera_all.isHidden = false
//        }
        
        let report_info = GlobalMembers.preferences.dictionary(forKey: GlobalMembers.reportinfoKey)! as NSDictionary
        let cxc_total_amount = report_info["cxc_total_amount"] as! Double
        let cxc_total_amount_expired = report_info["cxc_total_amount_expired"] as! Double
        
        self.lbl_client_cartera_normal.text = self.convertDoubleToCurrency(amount: cxc_total_amount)
        self.lbl_client_cartera_vencida.text = self.convertDoubleToCurrency(amount: cxc_total_amount_expired)
        
        let cartera_total = cxc_total_amount + cxc_total_amount_expired
        self.lbl_client_cartera_total.text = self.convertDoubleToCurrency(amount: cartera_total)
        
        
        let cxp_total_amount = report_info["cxp_total_amount"] as! Double
        let cxp_total_amount_expired = report_info["cxp_total_amount_expired"] as! Double
        
        
        self.lbl_proveedor_cartera_normal.text = self.convertDoubleToCurrency(amount: cxp_total_amount)
        self.lbl_proveedor_cartera_vencida.text = self.convertDoubleToCurrency(amount: cxp_total_amount_expired)
        let cartera_total_prov = cxp_total_amount + cxp_total_amount_expired
        self.lbl_proveedor_cartera_total.text = self.convertDoubleToCurrency(amount: cartera_total_prov)
        
    }
    // Get Info CXC y CXP
    func getClientsandSuppliersInfo(){
        let comp_id = saved_company["id"] as! Int
        SwiftSpinner.show("Cargando")
        invokeServ.requestReportByCompanyDatabyCompanyWith(company_id:String(comp_id)) { (response, error) in

            if error == nil
            {
               
                GlobalMembers.preferences.set(response!["cxc"], forKey: GlobalMembers.cxcinfoKey)
                GlobalMembers.preferences.set(response!["cxp"], forKey: GlobalMembers.cxpinfoKey)
                let didSave = GlobalMembers.preferences.synchronize()
                DispatchQueue.main.async
                {

                    SwiftSpinner.hide()
                }

            }

        }

    }
    

       
   

    func sum_total(sum1:Double,sum2:Double)->Double?{
        
        return sum1 + (sum2)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
             return menu_reports.count
    }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_reports", for: indexPath) as!   ReportesTViewCell
         

            let image = UIImage(named: "img_arrow")
            let checkmark  = UIImageView(frame:CGRect(x:0, y:0, width:(image?.size.width)!, height:(image?.size.height)!));
            checkmark.image = image
            cell.accessoryView = checkmark
      

        cell.contentView.superview!.backgroundColor = UIColor.white
            
            // cell.backgroundColor = UIColor.white
             let menu = menu_reports[indexPath.row] as! NSDictionary
             cell.lbl_title.text = menu.object(forKey: "name") as? String
             let name_img = menu.object(forKey: "date") as? String
             cell.lbl_update.text = name_img
              //cell.accessoryView!.backgroundColor = UIColor.white
             cell.selectedBackgroundView?.backgroundColor = UIColor.white
        

             
             return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let index = self.tv_reports.indexPathForSelectedRow{
            self.tv_reports.deselectRow(at: index, animated: true)
        }
        switch indexPath.row {
        case 0:
            self.performSegue(withIdentifier: "segue_tesoreria", sender: nil)
            break
        case 1:
            self.performSegue(withIdentifier: "segue_v_c_g", sender: nil)
            break
        default:
            break
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 103
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    
    func refreshToken()
    {
        SwiftSpinner.show("Actualizando token...")

        invokeServ.refreshTokenDataWith { (response, error) in
            SwiftSpinner.hide()
            if(response!)
            {
                
                self.getDataBank()
            }
        }
        
        
        
    }
  

}

extension UIView {
    @IBInspectable var shadow: Bool {
           get {
               return layer.shadowOpacity > 0.0
           }
           set {
               if newValue == true {
                   self.addShadow()
               }
           }
       }

   
   @IBInspectable var cornerRadius: CGFloat {
       get {
           return self.layer.cornerRadius
       }
       set {
           self.layer.cornerRadius = newValue

           // Don't touch the masksToBound property if a shadow is needed in addition to the cornerRadius
           if shadow == false {
               self.layer.masksToBounds = true
           }
       }
   }
    func addShadow(shadowColor: CGColor = UIColor.black.cgColor,
               shadowOffset: CGSize = CGSize(width: 1.0, height: 2.0),
               shadowOpacity: Float = 0.4,
               shadowRadius: CGFloat = 3.0)
    {
        layer.shadowColor = shadowColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
    }
    

 }
    


    

