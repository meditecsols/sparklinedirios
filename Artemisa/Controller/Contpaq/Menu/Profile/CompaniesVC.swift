//
//  CompaniesVC.swift
//  Artemisa
//
//  Created by Arturo Escamilla on 06/04/20.
//  Copyright © 2020 Microquasar. All rights reserved.
//

import UIKit
import SwiftSpinner

class CompaniesTViewCell: UITableViewCell
{
    @IBOutlet weak var lbl_company_name: UILabel!
    @IBOutlet weak var company_switch: UISwitch!
    
}
protocol ProfileVCDelegate {
   func resfreshdata()
}
class CompaniesVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var delegate: ProfileVCDelegate?
    
    let invokeServ = InvokeService()
    let alertmanager = AlertManager()
    
    var company_array = NSArray()

    @IBOutlet weak var tv_companies: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       

        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        tv_companies.isHidden = true
        self.tv_companies.backgroundView?.backgroundColor = UIColor.white
        self.tv_companies.backgroundColor = UIColor.white
        getcompany() 
    }
    
    func getcompany() {
        company_array = NSMutableArray()
         SwiftSpinner.show("Cargando...")
        invokeServ.requestCompaniesDataWith { (response, error) in
                        
                        if response != nil
                        {
                            self.company_array = response!
                            
                            DispatchQueue.main.async
                            {
                                if(self.company_array.count > 0)
                                {
                                    self.tv_companies.isHidden = false
                                }
                                SwiftSpinner.hide()
                                self.tv_companies.reloadData()
                            }
                            
                        }
                        else
                        {
                            DispatchQueue.main.async
                            {
                                self.refreshToken()
                                SwiftSpinner.hide()
                               
                            }
                            
                        }
                        
                        
                    }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return company_array.count
    }
       
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "company_cell", for: indexPath) as! CompaniesTViewCell
             cell.backgroundColor = UIColor.white
             let company = company_array[indexPath.row] as! NSDictionary
             cell.lbl_company_name.text = (company["short_name"] as! String)
        
            cell.company_switch.setOn(false, animated: true)
            cell.company_switch.tag = indexPath.row // for detect which row switch Changed
            cell.company_switch.addTarget(self, action: #selector(self.switchChanged(_:)), for: .valueChanged)

            let saved_company = GlobalMembers.preferences.dictionary(forKey: GlobalMembers.companyKey)
            if(saved_company != nil)
            {
                let comp_shortname = saved_company!["short_name"] as! String
                if(comp_shortname == cell.lbl_company_name.text)
                {
                    cell.company_switch.setOn(true, animated: true)
                }
            }
        
             
             return cell
    }
    @objc func switchChanged(_ sender : UISwitch!){
        
        let company = company_array[sender.tag] as! NSDictionary
        
        GlobalMembers.preferences.set(company, forKey: GlobalMembers.companyKey)
        
        let issaved =  GlobalMembers.preferences.synchronize()
        
        if(issaved)
        {
            self.tv_companies.reloadData()
        }
        if let pvc = self.presentationController {
            pvc.delegate?.presentationControllerWillDismiss?(pvc)
        }
        delegate?.resfreshdata()
        dismiss(animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 86
    }
    
    func refreshToken()
    {
        SwiftSpinner.show("Actualizando token...")

        invokeServ.refreshTokenDataWith { (response, error) in
            SwiftSpinner.hide()
            if(response!)
            {
                
                self.getcompany()
            }
        }
        
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
