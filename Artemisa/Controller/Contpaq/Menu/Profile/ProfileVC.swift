//
//  ProfileVC.swift
//  Artemisa
//
//  Created by Arturo Escamilla on 06/04/20.
//  Copyright © 2020 Microquasar. All rights reserved.
//

import UIKit
import SwiftSpinner

class ProfileTViewCell: UITableViewCell
{
    
    @IBOutlet weak var lbl_name: UILabel!



}
class ProfileVC: UIViewController,UITableViewDelegate, UITableViewDataSource,UIAdaptivePresentationControllerDelegate, ProfileVCDelegate {
   
     let alertmanager = AlertManager()
    @IBOutlet weak var lbl_version: UILabel!
    
    @IBOutlet weak var tv_profile: UITableView!
    @IBOutlet weak var lbl_email: UILabel!
    
    @IBOutlet weak var lbl_company_name: UILabel!
    
    var profile_optiions = NSMutableArray()
    
    var profile_dict = NSDictionary()
    var company_array = NSArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        let email = GlobalMembers.preferences.string(forKey: GlobalMembers.emailKey)
        lbl_email.text = email
        tv_profile.backgroundColor = UIColor.white
        
        let saved_company = GlobalMembers.preferences.dictionary(forKey: GlobalMembers.companyKey)
        if(saved_company != nil)
        {
            let comp_shortname = saved_company!["short_name"] as! String
            lbl_company_name.text = comp_shortname
            
        }
       
        
        //getcompany()
//        let opt1 = NSMutableDictionary()
//        opt1.setValue("Cambiar contraseña", forKey: "name")
//        profile_optiions.add(opt1)
        
        

        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        lbl_version.text = "Versión " + appVersion!
        profile_optiions = NSMutableArray()
        getprofile()
    }

    
    // Cargar tabla
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profile_optiions.count
    }
       
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "profile_cell", for: indexPath) as! ProfileTViewCell
             cell.backgroundColor = UIColor.white
             cell.tintColor = UIColor.white

             let image = UIImage(named: "img_arrow")
             let checkmark  = UIImageView(frame:CGRect(x:0, y:0, width:(image?.size.width)!, height:(image?.size.height)!));
             checkmark.image = image
             cell.accessoryView = checkmark
        
             let name = profile_optiions[indexPath.row] as! NSDictionary
             cell.lbl_name.text = name.object(forKey: "name") as? String

            if(indexPath.row == profile_optiions.count - 1)
            {
                cell.lbl_name.textColor = UIColor(red: 50/255.0, green: 118/255.0, blue: 177/255.0, alpha: 1.0)
            }
             
             return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

                
                if self.company_array.count >= 13
                {
            
                    switch indexPath.row
                    {
                        case 0:
                            self.performSegue(withIdentifier: "segue_companies", sender: nil)
                        break
//                        case 1:
//                            self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
//                        break
                        case 1:
            
                            let sessionAlert = UIAlertController(title: "Sparkline", message: "¿Deseas cerrar sessión?", preferredStyle: UIAlertController.Style.alert)

                            sessionAlert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { (action: UIAlertAction!) in
               
               
                                DispatchQueue.main.async {
                        //write your code here
                                    self.cleanpreferences()
                                }
                
                            }))

                            sessionAlert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: { (action: UIAlertAction!) in
           
                            }))

                            present(sessionAlert, animated: true, completion: nil)
           
                        break
                        default:
                        break
                    }
                }
                else
                {
                             switch indexPath.row
                             {
//                                case 0:
//                                    self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
//                                break
                                 case 0:
                     
                                     let sessionAlert = UIAlertController(title: "Sparkline", message: "¿Deseas cerrar sessión?", preferredStyle: UIAlertController.Style.alert)

                                     sessionAlert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { (action: UIAlertAction!) in
                        
                        
                                         DispatchQueue.main.async {
                                 //write your code here
                                             self.cleanpreferences()
                                         }
                         
                                     }))

                                     sessionAlert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: { (action: UIAlertAction!) in
                    
                                     }))

                                     present(sessionAlert, animated: true, completion: nil)
                    
                                 break
                                 default:
                                 break
                             }
                    
                }
        
        
        

        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66
    }
    
    func cleanpreferences(){
        GlobalMembers.preferences.set(nil, forKey: GlobalMembers.tokenKey)
        GlobalMembers.preferences.set(nil, forKey: GlobalMembers.emailKey)
        GlobalMembers.preferences.set(nil, forKey: GlobalMembers.companyKey)
        GlobalMembers.preferences.set(nil, forKey: GlobalMembers.tokenArtemisaKey)
        GlobalMembers.preferences.set(nil, forKey: GlobalMembers.emailArtemisaKey)
        GlobalMembers.preferences.set(nil, forKey: GlobalMembers.companyArtemisaKey)
        
        let didSave = GlobalMembers.preferences.synchronize()
        if didSave {
        // your code here
            self.dismiss(animated: true, completion: nil)
           
            
        }
    }
  
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "segue_companies" {
            
            if let presented = segue.destination as? CompaniesVC {
              segue.destination.presentationController?.delegate = self;
               presented.delegate = self
               //anything else you do already
            }
        }

    }
 
    public func presentationControllerDidDismiss(
      _ presentationController: UIPresentationController)
    {
      let saved_company = GlobalMembers.preferences.dictionary(forKey: GlobalMembers.companyKey)
      if(saved_company != nil)
      {
          let comp_shortname = saved_company!["short_name"] as! String
          lbl_company_name.text = comp_shortname
        
            if let index = self.tv_profile.indexPathForSelectedRow{
                self.tv_profile.deselectRow(at: index, animated: true)
            }
          
      }
     
    }
    func resfreshdata() {
        
        let saved_company = GlobalMembers.preferences.dictionary(forKey: GlobalMembers.companyKey)
             if(saved_company != nil)
             {
                 let comp_shortname = saved_company!["short_name"] as! String
                 lbl_company_name.text = comp_shortname
               
                   if let index = self.tv_profile.indexPathForSelectedRow{
                       self.tv_profile.deselectRow(at: index, animated: true)
                   }
                self.tabBarController!.selectedIndex = 0
                GlobalMembers.newCompany = true
                 
             }
      
    }
    
    func resfreshdataOneCompany() {
        
        let saved_company = GlobalMembers.preferences.dictionary(forKey: GlobalMembers.companyKey)
             if(saved_company != nil)
             {
                 let comp_shortname = saved_company!["short_name"] as! String
                 lbl_company_name.text = comp_shortname
               
                   if let index = self.tv_profile.indexPathForSelectedRow{
                       self.tv_profile.deselectRow(at: index, animated: true)
                   }
                //self.tabBarController!.selectedIndex = 0
                GlobalMembers.newCompany = true
                 
             }
      
    }
    
    func getprofile() {
        
         SwiftSpinner.show("Cargando...")
        invokeServ.requestProfileDatabyWith{ (response, error) in
                        
                        if response != nil
                        {
                            
                            self.profile_dict = response!
                            
                            invokeServ.requestCompaniesDataWith { (response, error) in
                                if response != nil
                                {
                                    self.company_array = NSArray()
                                    self.company_array = response!

                                    DispatchQueue.main.async
                                    {
                                       
                                          
                                            
                                                if self.company_array.count >= 13
                                                {
                                                    if(self.lbl_company_name.text!.count == 0)
                                                    {
                                                    self.alertmanager.alert(title: "Sparkline",mess: "Selecciona una empresa" , viewcontroller: self);
                                                    }
                                                    
                                                    let opt2 = NSMutableDictionary()
                                                     opt2.setValue("Seleccionar empresa", forKey: "name")
                                                     self.profile_optiions.add(opt2)
                                                    

                                                }
                                                else
                                                {
                                                    let company = self.company_array[0] as! NSDictionary
                                                    GlobalMembers.preferences.set(company, forKey: GlobalMembers.companyKey)
                                                    let issaved =  GlobalMembers.preferences.synchronize()
                                                    if(issaved)
                                                    {
                                                        self.resfreshdataOneCompany()
                                                    }
                                                }
//
//                                           let opt3 = NSMutableDictionary()
//                                           opt3.setValue("Salir de Contpaq", forKey: "name")
//                                           self.profile_optiions.add(opt3)
                                                                            
                                            
                                        
                                            let opt4 = NSMutableDictionary()
                                            opt4.setValue("Cerrar sesión", forKey: "name")
                                            self.profile_optiions.add(opt4)
                                        
                                       
                                    
                                        self.tv_profile.reloadData()
                                        SwiftSpinner.hide()
                                    }
                                    
                                    
                                }
                            }
                            
                            
                            
                            //self.profile_optiions = NSMutableArray()
                            
                            
                            
                        }
                        else
                        {
                            
                            DispatchQueue.main.async
                            {
                                self.refreshToken()
                                SwiftSpinner.hide()
                               
                            }
                            
                        }
                        
                        
                    }
        
    }
    
    
    func refreshToken()
    {
           SwiftSpinner.show("Actualizando token...")

           invokeServ.refreshTokenDataWith { (response, error) in
               SwiftSpinner.hide()
               if(response!)
               {
                   DispatchQueue.main.async
                   {
                    self.getprofile()
                   }
               }
           }
           
           
           
    }

}

    

