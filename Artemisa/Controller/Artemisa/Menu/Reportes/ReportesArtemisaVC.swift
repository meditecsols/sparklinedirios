//
//  ReportesArtemisaVC.swift
//  Artemisa
//
//  Created by Arturo Escamilla on 15/07/20.
//  Copyright © 2020 Microquasar. All rights reserved.
//

import UIKit
import WebKit
import SwiftSpinner

class ReportesArtemisaTViewCell: UITableViewCell
{
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_update: UILabel!
}

class ReportesArtemisaVC: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tv_reports: UITableView!
    
    var menu_reports = NSMutableArray()
    
    @IBOutlet weak var lbl_company_name: UILabel!
    
    
    
    @IBOutlet weak var lbl_client_cartera_normal: UILabel!
    @IBOutlet weak var lbl_proveedor_cartera_normal: UILabel!
    
    @IBOutlet weak var lbl_client_cartera_vencida: UILabel!
    @IBOutlet weak var lbl_proveedor_cartera_vencida: UILabel!
    
    @IBOutlet weak var lbl_client_cartera_total: UILabel!
    @IBOutlet weak var lbl_proveedor_cartera_total: UILabel!
    
    @IBOutlet weak var lbl_client_promesa: UILabel!
    @IBOutlet weak var lbl_proveedor_promesa: UILabel!
    
    @IBOutlet weak var lbl_proveedor_provisiones: UILabel!
    
    @IBOutlet weak var lbl_client_saldo_banc: UILabel!
    
    @IBOutlet weak var lbl_client_total: UILabel!
    @IBOutlet weak var lbl_proveedor_total: UILabel!
    
    @IBOutlet weak var view_cartera: UIView!
    @IBOutlet weak var view_total_cartera: UIView!
    
    @IBOutlet weak var view_promesas: UIView!
    @IBOutlet weak var view_promesas_total: UIView!
    
    
    let invokeServ = InvokeService()
    let alertmanager = AlertManager()
    
    var company_cxc = NSDictionary()
    var company_cxp = NSDictionary()
    var company_prov = NSMutableArray()
    var company_bank = NSMutableArray()
    
    let mxLocale = Locale(identifier: "es_MX")

    override func viewDidLoad() {
        super.viewDidLoad()
           // Background View
                   
           self.view_total_cartera.backgroundColor = UIColor(red: 153/255.0, green: 135/255.0, blue: 157/255.0, alpha: 1.0).withAlphaComponent(0.1)
           self.view_promesas_total.backgroundColor = UIColor(red: 153/255.0, green: 135/255.0, blue: 157/255.0, alpha: 1.0).withAlphaComponent(0.1)
        
                   

                   let opt1 = NSMutableDictionary()
                   opt1.setValue("Posición de tesorería", forKey: "name")
                   opt1.setValue("Actualizado el 24 de marzo", forKey: "date")
                   menu_reports.add(opt1)
                   
                   let opt2 = NSMutableDictionary()
                   opt2.setValue("Ventas, compras y gastos", forKey: "name")
                   opt2.setValue("Actualizado el 24 de marzo", forKey: "date")
                   menu_reports.add(opt2)
                   
                   let opt3 = NSMutableDictionary()
                   opt3.setValue("Utilidades", forKey: "name")
                   opt3.setValue("Actualizado el 24 de marzo", forKey: "date")
                   menu_reports.add(opt3)
                   
                   let opt4 = NSMutableDictionary()
                   opt4.setValue("Gastos", forKey: "name")
                   opt4.setValue("Actualizado el 24 de marzo", forKey: "date")
                   menu_reports.add(opt4)
                   
                   let opt5 = NSMutableDictionary()
                   opt5.setValue("Inventarios", forKey: "name")
                   opt5.setValue("Actualizado el 24 de marzo", forKey: "date")
                   menu_reports.add(opt5)

        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
               super.viewDidAppear(true)
            
                let saved_company = GlobalMembers.preferences.dictionary(forKey: GlobalMembers.companyArtemisaKey)
                if(saved_company != nil)
                {
                    let comp_shortname = saved_company!["company_shortname"] as! String
                    lbl_company_name.text = comp_shortname
                    
                    SwiftSpinner.show("Cargando...")
                              getDataCXC()
                    
                }
                else
                {
                    alertmanager.alert(title: "Sparkline",mess: "Selecciona una empresa" , viewcontroller: self);
                    self.tabBarController!.selectedIndex = 2
                }
              
        }
        
        func getDataCXC(){
            
            invokeServ.requestCXCDataWith { (response, error) in
                            
                            if error == nil
                            {
                                let cxc_array = response!["bi_cxc_sumary_by_company"] as! NSArray
                               

       
                                for cxc in cxc_array {

                                    let cxc_dict = cxc as! NSDictionary
                                    
                                    let saved_company = GlobalMembers.preferences.dictionary(forKey: GlobalMembers.companyArtemisaKey)
                                    if(saved_company != nil)
                                    {
                                        let comp_shortname = saved_company!["company_shortname"] as! String
                                        if(comp_shortname == cxc_dict["company_shortname"] as! String)
                                        {
                                            self.company_cxc = cxc_dict
                                        }
                                    }
                                    
                                            

                                }
                                DispatchQueue.main.async
                                {
                                   
                                    
                                    self.getDataCXP()
                                    
                                }
                                
                            }
                            else
                            {
                                 SwiftSpinner.hide()
                            }
                            
                            
                        }
            
        }
        
         func getDataCXP(){
            
            invokeServ.requestCXPDataWith { (response, error) in
                               
                               if error == nil
                               {
                                   
                                   let cxp_array = response!["bi_cxp_sumary_by_company"] as! NSArray
                               

                                  
                                 
                                   for cxp in cxp_array
                                   {

                                       let cxp_dict = cxp as! NSDictionary
                                    
                                        let saved_company = GlobalMembers.preferences.dictionary(forKey: GlobalMembers.companyArtemisaKey)
                                         if(saved_company != nil)
                                        {
                                            let comp_shortname = saved_company!["company_shortname"] as! String
                                            if(comp_shortname == cxp_dict["company_shortname"] as! String)
                                            {
                                               self.company_cxp = cxp_dict
                                            }
                                        }


                                   }
                                   DispatchQueue.main.async
                                   {
                                    self.getDataProvisions()
                                       //SwiftSpinner.hide()
                                       //self.filldata()
                                   }
                               }
                               else
                               {
                                 
                                SwiftSpinner.hide()
                                
                                
                               }
                               
                               
                           }
            
            
            
            
        }
        func getDataProvisions(){
            
            let saved_company = GlobalMembers.preferences.dictionary(forKey: GlobalMembers.companyArtemisaKey)
            if(saved_company != nil)
            {
                // Obtener id de compañia
                let comp_id = saved_company!["company_id"] as! String
                invokeServ.requestProvisionDataWith(company_id: comp_id) { (response, error) in
                    
                    if error == nil
                    {
                        let provisions_array = response!["bi_cxp_sumary_payment_provision"] as! NSArray
                        for prov in provisions_array
                        {

                            let prov_dict = prov as! NSDictionary
                         
                            self.company_prov.add(prov_dict)



                        }
                        DispatchQueue.main.async
                        {
                            self.getDataBank()
                        }
                        
                    }
                    else
                    {
                      
                     SwiftSpinner.hide()
                     
                     
                    }
                    
                }
                    
            }
           
            
            
        }
        
        func getDataBank(){
            
            let saved_company = GlobalMembers.preferences.dictionary(forKey: GlobalMembers.companyArtemisaKey)
            if(saved_company != nil)
            {
                // Obtener id de compañia
                let comp_id = saved_company!["company_id"] as! String
                invokeServ.requestBancosDataWith(company_id: comp_id) { (response, error) in
                    if error == nil
                    {
                        let bank_array = response!["bi_bks_sumary_currency_balance_by_company"] as! NSArray
                        for bank in bank_array
                        {

                            let bank_dict = bank as! NSDictionary
                         
                            self.company_bank.add(bank_dict)

                        }
                        DispatchQueue.main.async
                        {
                            SwiftSpinner.hide()
                            self.filldata()
                        }
                        
                        
                    }
                    else
                    {
                        SwiftSpinner.hide()
                    }
                }
            }
            
            
        }
            
           
        func filldata()  {
            
          
            
            if(company_cxc.count > 0)
            {
                    //Cartera Normal Cliente
                    let receivable_amount = company_cxc["receivable_amount"] as! String
                    let receivable_amount_double = Double(receivable_amount)
                    lbl_client_cartera_normal.text = convertDoubleToCurrency(amount: receivable_amount_double!)
                    //Cartera Vencida Cliente
                    let receivable_amount_expired = company_cxc["receivable_amount_expired"] as! String
                    let receivable_amount_expired_double = Double(receivable_amount_expired)
                    lbl_client_cartera_vencida.text =  convertDoubleToCurrency(amount: receivable_amount_expired_double!)
                    //Total Cartera Clientes
                    let total_clientes = sum_total(sum1: receivable_amount_double!, sum2: receivable_amount_expired_double!)
                    lbl_client_cartera_total.text = convertDoubleToCurrency(amount: total_clientes!)
                
                    let payment_promise_amount = company_cxc["payment_promise_amount"] as! String
                    let payment_promise_amount_double = Double(payment_promise_amount)
                    lbl_client_promesa.text = convertDoubleToCurrency(amount: payment_promise_amount_double!)
            }
            
            if(company_cxp.count > 0)
            {
                    let amount_to_pay = company_cxp["amount_to_pay"] as! String
                    let amount_to_pay_double = Double(amount_to_pay)
                    lbl_proveedor_cartera_normal.text = convertDoubleToCurrency(amount: amount_to_pay_double!)
                    
                    let receivable_amount_expired = company_cxp["amount_to_pay_expired"] as! String
                    let receivable_amount_expired_double = Double(receivable_amount_expired)
                        lbl_proveedor_cartera_vencida.text = convertDoubleToCurrency(amount: receivable_amount_expired_double!)
                    
                    let cartera_total = sum_total(sum1: amount_to_pay_double!, sum2: receivable_amount_expired_double!)
                    lbl_proveedor_cartera_total.text = convertDoubleToCurrency(amount: cartera_total!)
                
                    let payment_promise_amount = company_cxp["payment_promise_amount"] as! String
                    let payment_promise_amount_double = Double(payment_promise_amount)
                    lbl_proveedor_promesa.text = convertDoubleToCurrency(amount: payment_promise_amount_double!)
                    
            }
            
            //Total De Clientes Promesa
            
            var total_currency_balance_amount_double : Double = 0.00
            for dic_bank in company_bank {
                      let dic = dic_bank as! NSDictionary
                      let currency_balance_amount = dic["currency_balance_amount"] as! String
                      let currency_balance_amount_double = Double(currency_balance_amount)
                      total_currency_balance_amount_double = sum_total(sum1: total_currency_balance_amount_double, sum2: currency_balance_amount_double!)!
            }
                  
            lbl_client_saldo_banc.text = convertDoubleToCurrency(amount: total_currency_balance_amount_double)
            let  client_promesa_double = convertCurrencyToDouble(input: lbl_client_promesa.text!) ?? 0
            let client_saldo_banc_double = convertCurrencyToDouble(input: lbl_client_saldo_banc.text!) ?? 0
            let total_promesa_cliente = sum_total(sum1: client_promesa_double, sum2: client_saldo_banc_double)
            
            lbl_client_total.text = convertDoubleToCurrency(amount: total_promesa_cliente!)
            
            
            var total_payment_provision_amount_double : Double = 0.00
            for dic_prov in company_prov {
                      let dic = dic_prov as! NSDictionary
                      let payment_provision_amount = dic["payment_provision_amount"] as! String
                      let payment_provision_amount_double = Double(payment_provision_amount)
                      total_payment_provision_amount_double = sum_total(sum1: total_payment_provision_amount_double, sum2: payment_provision_amount_double!)!
            }
            
            lbl_proveedor_provisiones.text = convertDoubleToCurrency(amount: total_payment_provision_amount_double)
            
            let total_promesa_prov = sum_total(sum1: convertCurrencyToDouble(input: lbl_proveedor_promesa.text!) ?? 0, sum2: convertCurrencyToDouble(input: lbl_proveedor_provisiones.text!) ?? 0)
            
            lbl_proveedor_total.text = convertDoubleToCurrency(amount: total_promesa_prov!)
            
        }

        func sum_total(sum1:Double,sum2:Double)->Double?{
            
            return sum1 + (sum2)
            
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                 return menu_reports.count
        }
           
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell_reports", for: indexPath) as!   ReportesArtemisaTViewCell
             

                let image = UIImage(named: "img_arrow")
                let checkmark  = UIImageView(frame:CGRect(x:0, y:0, width:(image?.size.width)!, height:(image?.size.height)!));
                checkmark.image = image
                cell.accessoryView = checkmark
          

            cell.contentView.superview!.backgroundColor = UIColor.white
                
                // cell.backgroundColor = UIColor.white
                 let menu = menu_reports[indexPath.row] as! NSDictionary
                 cell.lbl_title.text = menu.object(forKey: "name") as? String
                 let name_img = menu.object(forKey: "date") as? String
                 cell.lbl_update.text = name_img
                  //cell.accessoryView!.backgroundColor = UIColor.white
                 cell.selectedBackgroundView?.backgroundColor = UIColor.white
            
    //            if indexPath.row % 2 == 0
    //            {
    //
    //                cell.contentView.backgroundColor = UIColor(red: 153/255.0, green: 135/255.0, blue: 157/255.0, alpha: 1.0).withAlphaComponent(0.1)
    //                //cell.contentView.superview!.backgroundColor = UIColor(red: 153/255.0, green: 135/255.0, blue: 157/255.0, alpha: 1.0).withAlphaComponent(0.1)
    //                cell.accessoryView!.backgroundColor = UIColor(red: 153/255.0, green: 135/255.0, blue: 157/255.0, alpha: 1.0).withAlphaComponent(0.1)
    //
    //
    //
    //            }
               
                 
                 return cell
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            if let index = self.tv_reports.indexPathForSelectedRow{
                self.tv_reports.deselectRow(at: index, animated: true)
            }
            switch indexPath.row {
            case 0:
                self.performSegue(withIdentifier: "segue_tesoreria_artemisa", sender: nil)
                break
            case 1:
                self.performSegue(withIdentifier: "segue_v_c_g", sender: nil)
                break
            default:
                break
            }
            
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 103
        }

        // MARK: - Navigation

        // In a storyboard-based application, you will often want to do a little preparation before navigation
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            // Get the new view controller using segue.destination.
            // Pass the selected object to the new view controller.
        }
      


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
