//
//  LoginArtemisaVC.swift
//  Artemisa
//
//  Created by Arturo Escamilla on 29/06/20.
//  Copyright © 2020 Microquasar. All rights reserved.
//

import UIKit
import SwiftSpinner



class LoginArtemisaVC: UIViewController {
    let invokeServ = InvokeService()
    let alertmanager = AlertManager()
    
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_password: UITextField!
    var company_array = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Looks for single or multiple taps.
               let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))

               //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
               //tap.cancelsTouchesInView = false

               view.addGestureRecognizer(tap)
               
               observeKeyboardNotifications()
               
               txt_email.attributedPlaceholder = NSAttributedString(string:"Usuario", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
               txt_password.attributedPlaceholder = NSAttributedString(string:"Contraseña", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        

        // Do any additional setup after loading the view.
    }
    
    //Calls this function when the tap is recognized.
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    fileprivate func observeKeyboardNotifications()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardShow), name:UIResponder.keyboardWillShowNotification , object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide), name:UIResponder.keyboardWillHideNotification , object: nil)
        
    }
    @objc func keyboardHide()
    {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.view.frame = CGRect(x: 0, y:0, width: self.view.frame.width, height: self.view.frame.height)
        }, completion: nil)
        
    }

    @objc func keyboardShow()
    {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.view.frame = CGRect(x: 0, y: -150, width: self.view.frame.width, height: self.view.frame.height)
        }, completion: nil)
    
    }
    
    @IBAction func enter_action(_ sender: Any) {
        
        if(txt_email.text?.count == 0)
        {
            alertmanager.alert(title: "Sparkline", mess: "Ingresa un correo", viewcontroller: self)
            return
        }
        else
        {
            if !isValidEmail(emailStr: txt_email.text!)
            {
                alertmanager.alert(title: "Sparkline",mess: "Ingresa un correo valido" , viewcontroller: self);
                return
            }
        }
        
        if txt_password.text?.count == 0
        {
            alertmanager.alert(title: "Sparkline",mess: "Ingresa una contraseña" , viewcontroller: self);
            return;
        }
        self.keyboardHide()
        SwiftSpinner.show("Entrando...")
        let user = txt_email.text!
        let password = txt_password.text!
               let dictParams: NSMutableDictionary? = ["email" : user,
                                                       "password" : password]
        
        invokeServ.requestForLoginArtemisaDataWith(dictParams!) { (result, error) in
            
                if error == nil
                {
                    let result_bool = result!["result"] as! Bool
                    if(!result_bool)
                    {
                        DispatchQueue.main.async
                        {
                            SwiftSpinner.hide()
                            self.alertmanager.alert(title: "Sparkline",mess: "Revisa tu usuario o contraseña" , viewcontroller: self);
                        }
                        return;
                        
                    }
                    //let dictaccesstoken: NSMutableDictionary? = ["access_token" : result!["access_token"] as Any , "token_type" : result!["token_type"] as Any]
                    let accesstoken = result!["token"] as! String
                    
                    GlobalMembers.preferences.set(accesstoken, forKey: GlobalMembers.tokenArtemisaKey)
                    GlobalMembers.preferences.set(user, forKey: GlobalMembers.emailArtemisaKey)
                    
                    let didSave = GlobalMembers.preferences.synchronize()

                    if didSave {
                        
                        if GlobalMembers.preferences.object(forKey: GlobalMembers.tokenArtemisaKey) != nil {
                            

                                    DispatchQueue.main.async
                                    {
                                        let dictParams: NSMutableDictionary? = ["user_name" : user,
                                                                                "token_app_notify" : GlobalMembers.tokenNotifArtemisa]
                                        
                                        let invokeServ2 = InvokeService()
                                        invokeServ2.requestUpdateTokenUserArtemisaDataWith(dictParams!) { (response, error) in
                                            
                                            DispatchQueue.main.async
                                            {
                                                SwiftSpinner.hide()
                                                self.performSegue(withIdentifier: "segue_menu_artemisa", sender: nil)
                                            }
                                            
                                        }
                                        
                                       

                                    }
                           
                                
                                
                                
                            }
                       }
                        
                    
                }
            
            
            
            
            
        
            }
        
        
        
    }
    
    func isValidEmail(emailStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 
    }
    
    @IBAction func back_action(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
   

}
